package tv.loqoo.FourthScreen.Fragments;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragMsg1 extends Fragment {
	
	String fragFooter;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragfooter,
        container, false);
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
  super.onActivityCreated(savedInstanceState);
  Bundle args = getArguments();
  TextView txt = (TextView)getView().findViewById(R.id.fragFooter);
  txt.setSelected(true);
  fragFooter = args.getString("fragFooter");
  txt.setText(fragFooter);
  
  }

} 