package tv.loqoo.FourthScreen.Fragments;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.R.layout;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

public class FragExtra extends Fragment {
   
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragextra,
        container, false);
    return view;
  }


} 