package tv.loqoo.FourthScreen.Fragments.Scenes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import tv.loqoo.FourthScreen.LoqooBroadcast;
import tv.loqoo.FourthScreen.Music;
import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.SceneState;
import tv.loqoo.FourthScreen.Video;
import tv.loqoo.FourthScreen.Music.DownloadFile;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback.EmptyCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ImageView.ScaleType;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class FragWebView extends Fragment {
	
	String TAG = "loqootv";
	MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
	MediaPlayer player;	
	ProgressBar progress;
	DownloadFile test;
	String sceneUrl;
	String profileName;
	String profileImage;
	String sceneTITLE;
	String sceneVIEWS;
	String sceneLIKES;
	String sceneTHUMBNAIL;
	String sceneDURATION;
	String sceneProvider;
    String sceneCHANNEL;
    String sceneACTION;
	SceneState sceneState = new SceneState();	
	private OnFragWebViewListener mListener;
	@InjectView(R.id.fragWebView)
	WebView fragWeb;
	@InjectView(R.id.fragWebProgressBar)
	ProgressBar fragWebProgressBar;

    public static FragWebView  newInstance(JSONObject message)
    {
        FragWebView fragWebView = new FragWebView();

        // arguments
        
        Bundle arguments = new Bundle();
        arguments.putString("json",message.toString());
        fragWebView.setArguments(arguments);
        

        return fragWebView;
    }

    public FragWebView() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragWebViewListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragWebViewListener");
        }
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragwebview,
        container, false);
    Window w = getActivity().getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
	ButterKnife.inject(this, view);
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    Bundle args = getArguments();
    Log.d("LifeCycle.Image","OnActivityCreate");
	String json = args.getString("json");
	JSONObject backToJson = null;
	try {
		backToJson = new JSONObject(json);
		sceneUrl = backToJson.getString("url");
		Log.v(TAG+"sceneUrl", sceneUrl);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Log.v(TAG+"sceneURLOUT", sceneUrl);
	
	fragWeb.setWebViewClient(new FragWebViewBrowser());
	fragWeb.setWebChromeClient(new WebChromeClient());
    fragWeb.getSettings().setLoadsImagesAutomatically(true);
    fragWeb.getSettings().setJavaScriptEnabled(true);
    fragWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    fragWeb.getSettings().setBuiltInZoomControls(false); 
    fragWeb.getSettings().setSupportZoom(false);
    fragWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);   
    fragWeb.getSettings().setAllowFileAccess(true); 
    fragWeb.getSettings().setDomStorageEnabled(true);
	fragWeb.loadUrl(sceneUrl);
	
			

}
  
  public class FragWebViewBrowser extends WebViewClient {
	  
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
      if (url.endsWith(".mp3")) {
          Intent intent = new Intent(Intent.ACTION_VIEW);
          intent.setDataAndType(Uri.parse(url), "audio/*");
          view.getContext().startActivity(intent);   
          return true;
      } else if (url.endsWith(".mp4") || url.endsWith(".3gp")) {
              Intent intent = new Intent(Intent.ACTION_VIEW); 
              intent.setDataAndType(Uri.parse(url), "video/*");
              view.getContext().startActivity(intent);   
              return true;
      } else {
          return super.shouldOverrideUrlLoading(view, url);
      }
      }
      
      @Override
		public void onPageFinished(WebView view, String url) {
    	  		fragWebProgressBar.setVisibility(View.GONE);
				fragWebProgressBar.setProgress(100);
			super.onPageFinished(view, url);
		}

		 @Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			 fragWebProgressBar.setVisibility(View.VISIBLE);
			 fragWebProgressBar.setProgress(0);
			super.onPageStarted(view, url, favicon);
		}
  }

protected void onStart(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d("LifeCycle.Image","ONstart");

	}



  
  @Override
  public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
  	Animator animator = null;
      // In this example, i want to add a listener when the card_flip_right_in animation
      // is about to happen.
      if (nextAnim == R.anim.enter) {
          animator = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
          // * Sometimes onCreateAnimator will be called and nextAnim will be 0, 
          //   causing animator to be null.
          // * I wanted to add a listener when the fragment was entering - 
          //   your use case may be different.
          if (animator != null && enter) {

              animator.addListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animation) {
                	  Log.d("fragImage","animation start");
                  }

                  @Override
                  public void onAnimationEnd(Animator animation) {
                	  Log.d("fragImage","animation over");
                  }

                  @Override
                  public void onAnimationCancel(Animator animation) {
                  }

                  @Override
                  public void onAnimationRepeat(Animator animation) {
                  }
              });
          }
      }
      return animator;
  }
  
  public interface OnFragWebViewListener {

      public void onSceneEnd(String arg1);
  }
  
  @Override
  public void onDestroyView() {
      super.onDestroyView();
      Log.v(TAG + "onDestroyFRAGImage", "onDestroyFRAGImage");
  }

  
  @Override
  public void onDetach(){

      super.onDetach();
      Log.v(TAG + "onDetachFRAGImage", "onDetachFRAGImage");

      fragWeb.removeAllViews();
      fragWeb.destroy();
  }
  
  
  }