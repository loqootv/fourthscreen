package tv.loqoo.FourthScreen.Fragments.Scenes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import tv.loqoo.FourthScreen.Music;
import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.SceneState;
import tv.loqoo.FourthScreen.Music.DownloadFile;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.PubnubServices;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class FragAudio extends Fragment {
	
	String TAG = "loqootv";
	MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
	MediaPlayer player;	
	ImageView image;
	ProgressBar progress;
	DownloadFile test;
	String sceneUrl;
	String profileName;
	String profileImage;
	String sceneTITLE;
	String sceneVIEWS;
	String sceneLIKES;
	String sceneTHUMBNAIL;
	String sceneDURATION;
	String sceneProvider;
	String sceneCHANNEL;
	String sceneACTION;
	SceneState sceneState = new SceneState();	
	private OnFragAudioListener mListener;

    public static FragAudio  newInstance(String d)
    {
        FragAudio fragAudio = new FragAudio();

        // arguments
        Bundle arguments = new Bundle();
        fragAudio.setArguments(arguments);

        return fragAudio;
    }

    public FragAudio() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragAudioListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragAudioListener");
        }
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragaudio,
        container, false);
    Window w = getActivity().getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_IMMERSIVE;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
    View decorView = getActivity().getWindow().getDecorView();
    decorView.setOnSystemUiVisibilityChangeListener
    (new View.OnSystemUiVisibilityChangeListener() {
@Override
public void onSystemUiVisibilityChange(int visibility) {
    // Note that system bars will only be "visible" if none of the
    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
        // TODO: The system bars are visible. Make any desired
        // adjustments to your UI, such as showing the action bar or
        // other navigational controls.
    } else {
        // TODO: The system bars are NOT visible. Make any desired
        // adjustments to your UI, such as hiding the action bar or
        // other navigational controls.
    }
}
});
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    Bundle args = getArguments();
    image = (ImageView)getView().findViewById(R.id.imageView1);
    Log.d("LifeCycle.Audio","OnActivityCreate");
	String sceneUrl = args.getString("url");
	Log.d("fragAUDIO", sceneUrl);
    sceneProvider = args.getString("sceneProvider");
    profileName = args.getString("profileName");
    profileImage = args.getString("profileImage");
    sceneTITLE = args.getString("sceneTITLE");
    sceneVIEWS = args.getString("sceneVIEWS");
    sceneLIKES = args.getString("sceneLIKES");
    sceneDURATION = args.getString("sceneDURATION");
    sceneCHANNEL = args.getString("sceneCHANNEL");
    sceneACTION = args.getString("sceneACTION");
    

    //downBtn = (Button)findViewById(R.id.downloadBtn);
    progress = (ProgressBar)getView().findViewById(R.id.progressBar1);
    test = new DownloadFile();
    progress.setVisibility(View.VISIBLE);
    
   
    
	Map<String, String> meta = new HashMap<String,String>();
	 if (Build.VERSION.SDK_INT >= 14)
     	metaRetriever.setDataSource(sceneUrl, new HashMap<String, String>());
	if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)!=null){ 
		meta.put("Artist",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)!=null){ 
		meta.put("Title",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)!=null){ 
	meta.put("Duration",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM)!=null){ 
		meta.put("Album",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
	}
	
	String artistName = meta.get("Artist");
    String songTitle = meta.get("Title");
    String songDuration = meta.get("Duration");

	
    playMusic(sceneUrl);
    image.setScaleType(ScaleType.FIT_CENTER);
    Picasso.with(getActivity()).load(sceneTHUMBNAIL).into(image);
}

protected void onStart(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d("LifeCycle.Music","ONstart");

	}

public class DownloadFile extends AsyncTask<String,Void,Void> {

    private Context context;
    public void setContext(Context contextf){
        context = contextf;
    }

    @Override
    protected Void doInBackground(String... arg0) {
        try {
            URL url = new URL(arg0[0]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            //c.setDoOutput(true);
            c.connect();

            File sdcard = Environment.getExternalStorageDirectory();
            File myDir = new File(sdcard,"Android/data/tv.loqoo.FourthScreen/temp");
            myDir.mkdirs();
            File outputFile = new File(myDir, "music.mp3");
            if(outputFile.exists()){
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.flush();
            fos.close();
            is.close();


        } catch (Exception e) {
            Log.e("UpdateAPP", "Update error! " + e.getMessage());
        }
        return null;
    }

	}


private Bitmap downloadBitmap(final String url) {
    metaRetriever.setDataSource(url, new HashMap<String, String>());
    try {
       final byte[] art = metaRetriever.getEmbeddedPicture();
       metaRetriever.release();
       return BitmapFactory.decodeByteArray(art, 0, art.length);
    } catch (Exception e) {
       Log.e("LOGTAG", "Couldn't create album art: " + e.getMessage());
       return BitmapFactory.decodeResource(getResources(), R.drawable.ltvrobots);
       
    }
 }



	

public void playMusic(final String sceneUrl) {
	try {
		player = new MediaPlayer();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setDataSource(sceneUrl);
        player.setLooping(false);
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {            
            
            @Override
            public void onPrepared(MediaPlayer mp) {
            	progress.setVisibility(View.INVISIBLE);
            	//TODO playing();
    			//LogToQueuewChannel("A Scene You Aired on #"+channelName" Now Playing on"+,networkName, "sceneIsNowPlaying")
            	
                mp.start();
                PubnubServices pubnubServices = new PubnubServices(getActivity());                                             
                new DeviceInfo(getActivity());   
                String type = "nowPlaying";
                JSONObject subdata = DeviceInfo.getSubscriberData(getActivity());
                try{                                                      
                        subdata.put("nowPlaying", true);
                        subdata.put("type", type);
                        subdata.put("SceneProvider", sceneProvider);
                        subdata.put("profileName", profileName);
                        subdata.put("profileImage", profileImage);
                        subdata.put("sceneTITLE", sceneTITLE);
                        subdata.put("sceneVIEWS", sceneVIEWS);
                        subdata.put("sceneLIKES", sceneLIKES);
                        subdata.put("sceneDURATION", sceneDURATION);
                        subdata.put("sceneCHANNEL", sceneCHANNEL);
                        subdata.put("sceneACTION",  sceneACTION);
                }catch(JSONException e){                                  
                        Log.v(TAG+"AUDIOonPrepared",e.toString());   
                };                                                        
                pubnubServices.sendmsgJSON("pubnubController", subdata);
                //sceneState.AUDIOIsPlaying(Music.this);

         }
        });
        
        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
            	Log.e("MEDIAPLAYER ERRORS",
                        "what: " + what + "  extra: "   + extra);
            	mp.reset();
            	try {
					mp.setDataSource(sceneUrl);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	try {
					mp.prepare();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                return true;
            }
        });
        
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
        	  @Override
        	  public void onCompletion(MediaPlayer mp) {
        		//sceneState.AUDIOIsNotPlaying(Music.this);
        		  mp.release();
        		 //TODO notPlaying();
              	 // Intent welcome = new Intent(Music.this,TV.class);
                 // Music.this.startActivity(welcome);
        		 //getActivity().getFragmentManager().popBackStack();
        		  PubnubServices pubnubServices = new PubnubServices(getActivity());                                             
                  new DeviceInfo(getActivity());
                  String type = "mediaComplete";
                  JSONObject subdata = DeviceInfo.getSubscriberData(getActivity());
                  try{    
                  	    subdata.put("type", type);
                  	    subdata.put("SceneProvider", sceneProvider);
                          subdata.put("profileName", profileName);
                          subdata.put("profileImage", profileImage);
                          subdata.put("sceneTITLE", sceneTITLE);
                          subdata.put("sceneVIEWS", sceneVIEWS);
                          subdata.put("sceneLIKES", sceneLIKES);
                          subdata.put("sceneDURATION", sceneDURATION);
                          subdata.put("sceneCHANNEL", sceneCHANNEL);
                          subdata.put("sceneACTION", sceneACTION);
                          subdata.put("mediaComplete", true);                   
                  }catch(JSONException e){                                  
                          Log.v(TAG+"AUDIOonCompletion",e.toString());   
                  };                                                        
                  pubnubServices.sendmsgJSON("pubnubController", subdata);
                  
        	    
        	  }
        	});
    player.prepareAsync();
    
    
    
}catch (Exception e) {
    e.printStackTrace();
    // TODO: handle exception


}
	
	
}

public void playing(){
	
}

public void notPlaying(){
		
}

  
  @Override
  public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
  	Animator animator = null;
      // In this example, i want to add a listener when the card_flip_right_in animation
      // is about to happen.
      if (nextAnim == R.anim.enter) {
          animator = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
          // * Sometimes onCreateAnimator will be called and nextAnim will be 0, 
          //   causing animator to be null.
          // * I wanted to add a listener when the fragment was entering - 
          //   your use case may be different.
          if (animator != null && enter) {

              animator.addListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animation) {
                	  Log.d("fragAUDIO","animation start");
                  }

                  @Override
                  public void onAnimationEnd(Animator animation) {
                	  Log.d("fragAUDIO","animation over");
                  }

                  @Override
                  public void onAnimationCancel(Animator animation) {
                  }

                  @Override
                  public void onAnimationRepeat(Animator animation) {
                  }
              });
          }
      }
      return animator;
  }
  
  public interface OnFragAudioListener {
 
      public void onSceneEnd(String arg1);
  }
  
  @Override
  public void onDestroyView() {
      super.onDestroyView();
      Log.v(TAG + "onDestroyFRAGAUDIO", "onDestroyFRAGAUDIO");
  }

  @Override
  public void onDetach() {
      super.onDetach();
      Log.v(TAG + "onDetachFRAGAUDIO", "onDetachFRAGAUDIO");

  }
  
  
  }