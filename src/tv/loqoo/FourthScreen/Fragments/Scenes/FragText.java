package tv.loqoo.FourthScreen.Fragments.Scenes;

import tv.loqoo.FourthScreen.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class FragText extends Fragment {

	String TAG = "loqootv";
	String KEY_URL = "";
	String url;
    private OnFragTextListener mListener;
    
    public static FragText  newInstance(String d)
    {
        FragText fragText = new FragText();

        // arguments
        Bundle arguments = new Bundle();
        fragText.setArguments(arguments);

        return fragText;
    }

    public FragText() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragTextListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragTextListener");
        }
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragtext,
        container, false);
    
    Bundle arguments = getArguments();
    url = arguments.getString(KEY_URL);

    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    
    
  
  //ImageView centerLogo = (ImageView) getActivity().findViewById(R.id.frag_centerLogo);
  //Animation pulse = AnimationUtils.loadAnimation(this.getActivity(), R.anim.pulse);
  //centerLogo.startAnimation(pulse);
  

  
  }
  
  public interface OnFragTextListener {
      public void onSceneEnd(String arg1);
  }

} 