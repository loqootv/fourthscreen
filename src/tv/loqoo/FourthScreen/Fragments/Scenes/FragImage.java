package tv.loqoo.FourthScreen.Fragments.Scenes;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import tv.loqoo.FourthScreen.LoqooBroadcast;
import tv.loqoo.FourthScreen.Music;
import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.SceneState;
import tv.loqoo.FourthScreen.Video;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragImage.OnFragImageListener;
import tv.loqoo.FourthScreen.Music.DownloadFile;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback.EmptyCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ImageView.ScaleType;

public class FragImage extends Fragment {
	
	String TAG = "loqootv";
	MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
	MediaPlayer player;	
	ProgressBar progress;
	DownloadFile test;
	String sceneUrl;
	String profileName;
	String profileImage;
	String sceneTITLE;
	String sceneVIEWS;
	String sceneLIKES;
	String sceneTHUMBNAIL;
	String sceneDURATION;
	String sceneProvider;
    String sceneCHANNEL;
    String sceneACTION;
	SceneState sceneState = new SceneState();	
	private OnFragImageListener mListener;

    public static FragImage  newInstance(String d)
    {
        FragImage fragImage = new FragImage();

        // arguments
        Bundle arguments = new Bundle();
        fragImage.setArguments(arguments);

        return fragImage;
    }

    public FragImage() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragImageListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragImageListener");
        }
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragimage,
        container, false);
    Window w = getActivity().getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    Bundle args = getArguments();
    Log.d("LifeCycle.Image","OnActivityCreate");
	sceneUrl = args.getString("url");
	Log.d("fragImage", sceneUrl);
    sceneProvider = args.getString("sceneProvider");
    profileName = args.getString("profileName");
    profileImage = args.getString("profileImage");
    sceneTITLE = args.getString("sceneTITLE");
    sceneVIEWS = args.getString("sceneVIEWS");
    sceneLIKES = args.getString("sceneLIKES");
    sceneDURATION = args.getString("sceneDURATION");
    sceneCHANNEL = args.getString("sceneCHANNEL");
    sceneACTION = args.getString("sceneACTION");
	ImageView image = (ImageView)getView().findViewById(R.id.imageView1);
	image.setScaleType(ScaleType.FIT_CENTER);
	ProgressBar progressBar = (ProgressBar)getView().findViewById(R.id.progressBar1);
	Log.d("Image.incomingScenePicasso",sceneUrl);
	//av = (ImageView)findViewById(R.id.im);
	//Picasso.with(this).load(networkAvatar).into(av);	
}

protected void onStart(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d("LifeCycle.Image","ONstart");

	}



  
  @Override
  public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
  	Animator animator = null;
      // In this example, i want to add a listener when the card_flip_right_in animation
      // is about to happen.
      if (nextAnim == R.anim.enter) {
          animator = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
          // * Sometimes onCreateAnimator will be called and nextAnim will be 0, 
          //   causing animator to be null.
          // * I wanted to add a listener when the fragment was entering - 
          //   your use case may be different.
          if (animator != null && enter) {

              animator.addListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animation) {
                	  Log.d("fragImage","animation start");
                  }

                  @Override
                  public void onAnimationEnd(Animator animation) {
                	  Log.d("fragImage","animation over");
                  }

                  @Override
                  public void onAnimationCancel(Animator animation) {
                  }

                  @Override
                  public void onAnimationRepeat(Animator animation) {
                  }
              });
          }
      }
      return animator;
  }
  
  public interface OnFragImageListener {

      public void onSceneEnd(String arg1);
  }
  
  @Override
  public void onDestroyView() {
      super.onDestroyView();
      Log.v(TAG + "onDestroyFRAGImage", "onDestroyFRAGImage");
  }

  @Override
  public void onDetach() {
      super.onDetach();
      Log.v(TAG + "onDetachFRAGImage", "onDetachFRAGImage");

  }
  
  
  }