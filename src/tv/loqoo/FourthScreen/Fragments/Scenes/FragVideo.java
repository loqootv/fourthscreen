package tv.loqoo.FourthScreen.Fragments.Scenes;

import org.json.JSONException;
import org.json.JSONObject;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.SceneState;
import tv.loqoo.FourthScreen.Video;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragVideo.OnFragVideoListener;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.PubnubServices;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class FragVideo extends Fragment {
	
	String TAG = "loqootv";
	VideoView video;
	SceneState sceneState = new SceneState();
	private static Intent intent;
    String sceneProvider;
    String profileName;
    String profileImage;
    String sceneTITLE;
    String sceneVIEWS;
    String sceneLIKES;
    String sceneDURATION;
    String sceneCHANNEL;
    String sceneACTION;
    private OnFragVideoListener mListener;
    String pubnubController = getActivity().getResources().getString(R.string.pubnubControllerDEVMODE);


    public static FragVideo  newInstance(String d)
    {
        FragVideo fragVideo = new FragVideo();

        // arguments
        Bundle arguments = new Bundle();
        fragVideo.setArguments(arguments);

        return fragVideo;
    }

    public FragVideo() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragVideoListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragVideoListener");
        }
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragvideo,
        container, false);
    
    Window w = getActivity().getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_IMMERSIVE;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
    View decorView = getActivity().getWindow().getDecorView();
    decorView.setOnSystemUiVisibilityChangeListener
    (new View.OnSystemUiVisibilityChangeListener() {
@Override
public void onSystemUiVisibilityChange(int visibility) {
    // Note that system bars will only be "visible" if none of the
    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
        // TODO: The system bars are visible. Make any desired
        // adjustments to your UI, such as showing the action bar or
        // other navigational controls.
    } else {
        // TODO: The system bars are NOT visible. Make any desired
        // adjustments to your UI, such as hiding the action bar or
        // other navigational controls.
    }
}


});
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    getView().post(new Runnable() {
        @Override
        public void run() {

          // code you want to run when view is visible for the first time
        }
      }
    );
    
    Bundle args = getArguments();
    video = (VideoView)getView().findViewById(R.id.videoView);
  	Log.d("LifeCycle.Video","FirstIntent");
  	String sceneUrl = this.getArguments().getString("url");	
    String sceneProvider = args.getString("scenePROVIDER");
    String profileName = this.getArguments().getString("profileNAME");
    //String profileImage = args.getString("profileIMAGE");
    String sceneTITLE = args.getString("sceneTITLE");
    String sceneVIEWS = args.getString("sceneVIEWS");
    String sceneLIKES = args.getString("sceneLIKES");
    String sceneDURATION = args.getString("sceneDURATION");
    String scenebody = args.getString("body");
    String sceneCHANNEL = args.getString("sceneCHANNEL");
    String sceneACTION = args.getString("sceneACTION");
    //Log.d("fragVIDEO",networkName);
    //Log.v("video_url", sceneProvider);
    new video().execute(sceneUrl);
}
  
  class video extends AsyncTask<String, Uri, Void> {
	    Integer track = 0;
	    ProgressDialog dialog;
	    MediaController media;

	    protected void onPreExecute() {
	        dialog = new ProgressDialog(getActivity());
	        dialog.setMessage("Loading, Please Wait...");
	        dialog.setCancelable(true);
	        dialog.show();
	    }

	    protected void onProgressUpdate(final Uri... uri) {

	        try {

	            media = new MediaController(getActivity());
	            video.setMediaController(media);
	            media.setPrevNextListeners(new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {
	                    // next button clicked

	                }
	            }, new View.OnClickListener() {
	                @Override
	                public void onClick(View v) {

	                }
	            });
	            media.show(10000);

	            video.setVideoURI(uri[0]);
	            video.requestFocus();
	            video.setOnPreparedListener(new OnPreparedListener() {

	                public void onPrepared(MediaPlayer arg0) {

	            		sceneState.videoIsPlaying(getActivity());
	            	    int videoWidth = video.getWidth();
	            	    int videoHeight = video.getHeight();
	                    video.start();
	                    mListener.isPlaying(1);
	                    PubnubServices pubnubServices = new PubnubServices(getActivity());                                             
	                    new DeviceInfo(getActivity());   
	                    String type = "nowPlaying";
	                    JSONObject subdata = DeviceInfo.getSubscriberData(getActivity());
	                    try{                                                      
	                            subdata.put("nowPlaying", true);
	                            subdata.put("type", type);
	                            subdata.put("SceneProvider", sceneProvider);
	                            subdata.put("profileName", profileName);
	                            subdata.put("profileImage", profileImage);
	                            subdata.put("sceneTITLE", sceneTITLE);
	                            subdata.put("sceneVIEWS", sceneVIEWS);
	                            subdata.put("sceneLIKES", sceneLIKES);
	                            subdata.put("sceneDURATION", sceneDURATION);
	                            subdata.put("sceneCHANNEL", sceneCHANNEL);
	                            subdata.put("sceneACTION", sceneACTION);
	                    }catch(JSONException e){                                  
	                            Log.v(TAG+"VIDEOonPrepared",e.toString());   
	                    };                                                        
	                    pubnubServices.sendmsgJSON(pubnubController, subdata);
	                    dialog.dismiss();
	                }
	            });
	            
	            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
	          	  @Override
	          	  public void onCompletion(MediaPlayer mp) {
	          		getActivity().getFragmentManager().popBackStack();
	          		PubnubServices pubnubServices = new PubnubServices(getActivity());                                             
                    new DeviceInfo(getActivity());
                    String type = "sceneEnd";
                    JSONObject subdata = DeviceInfo.getSubscriberData(getActivity());
                    try{    
                    	    subdata.put("type", type);
                    	    subdata.put("SceneProvider", sceneProvider);
                            subdata.put("profileName", profileName);
                            subdata.put("profileImage", profileImage);
                            subdata.put("sceneTITLE", sceneTITLE);
                            subdata.put("sceneVIEWS", sceneVIEWS);
                            subdata.put("sceneLIKES", sceneLIKES);
                            subdata.put("sceneDURATION", sceneDURATION);
                            subdata.put("sceneCHANNEL", sceneCHANNEL);
                            subdata.put("sceneACTION", sceneACTION);                 
                    }catch(JSONException e){                                  
                            Log.v(TAG+"VIDEOonCompletion",e.toString());   
                    };                                                        
                    pubnubServices.sendmsgJSON(pubnubController, subdata);     
	          		mListener.onSceneEnd("1");
	          		    
	          	  }
	          	});
	            
	            video.setOnErrorListener(new MediaPlayer.OnErrorListener()
	            {
	               @Override
	                public boolean onError(MediaPlayer videostream, int what, int extra)
	                {

	            	    dialog.dismiss();
	                    return false;
	                }           
	            }) ;
	             

	        } catch (IllegalArgumentException e) {
	            e.printStackTrace();
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        } catch (SecurityException e) {
	            /// TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	         

	    }

	    @Override
	    protected Void doInBackground(String... params) {
	        try {
	            Uri uri = Uri.parse(params[0]);
	            publishProgress(uri);
	        } catch (Exception e) {
	            e.printStackTrace();

	        }

	        return null;
	    }
	    
	}


	public void playVideo() {
		intent = Video.instance();
		String sceneUrl = intent.getStringExtra("url");
		String networkName = intent.getStringExtra("networkName");
		String networkAvatar = intent.getStringExtra("networkAvatar");
		String sceneDescp = intent.getStringExtra("sceneDescp");
		String sceneTag1 = intent.getStringExtra("sceneTag1");
		String sceneTag3 = intent.getStringExtra("sceneTag3");
	    new video().execute(sceneUrl);
	}
  
  @Override
  public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
  	Animator animator = null;
      // In this example, i want to add a listener when the card_flip_right_in animation
      // is about to happen.
      if (nextAnim == R.anim.enter) {
          animator = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
          // * Sometimes onCreateAnimator will be called and nextAnim will be 0, 
          //   causing animator to be null.
          // * I wanted to add a listener when the fragment was entering - 
          //   your use case may be different.
          if (animator != null && enter) {

              animator.addListener(new Animator.AnimatorListener() {
                  @Override
                  public void onAnimationStart(Animator animation) {
                	  Log.d("fragVIDEO","animation start");
                  }

                  @Override
                  public void onAnimationEnd(Animator animation) {
                	  Log.d("fragVIDEO","animation over");
                  }

                  @Override
                  public void onAnimationCancel(Animator animation) {
                  }

                  @Override
                  public void onAnimationRepeat(Animator animation) {
                  }
              });
          }
      }
      return animator;
  }
  
  public interface OnFragVideoListener {
      
      public void isPlaying(int playing);

      public void onSceneEnd(String arg1);
      
  }
  
  @Override
  public void onDestroyView() {
      super.onDestroyView();
      Log.v(TAG + "onDestroyFRAGVIDEO", "onDestroyFRAGVIDEO");
  }

  @Override
  public void onDetach() {
      super.onDetach();
      Log.v(TAG + "onDetachFRAGVIDEO", "onDetachFRAGVIDEO");

  }
  
  
  }