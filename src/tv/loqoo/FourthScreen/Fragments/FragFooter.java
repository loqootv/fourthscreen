package tv.loqoo.FourthScreen.Fragments;


import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import android.app.Fragment;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragFooter extends Fragment {
	
	String TAG = "loqootv";
	String fragFooter;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragfooter,
        container, false);
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
  super.onActivityCreated(savedInstanceState);
  Bundle args = getArguments();
  Animation mAnimation = new TranslateAnimation(2000, -3000, 
          1, 1);
  mAnimation.setDuration(60000); 
  mAnimation.setRepeatMode(Animation.RESTART);
  mAnimation.setRepeatCount(Animation.INFINITE);
  TextView txt = (TextView)getView().findViewById(R.id.fragFooter);
  
  
  txt.setSelected(true);
  txt.setAnimation(mAnimation);
  txt.requestFocus();
  
  txt.setMovementMethod(new ScrollingMovementMethod());
  fragFooter = args.getString("fragFooter");
  txt.setText(fragFooter);
  
  
  }

} 