package tv.loqoo.FourthScreen.Fragments;

import com.squareup.picasso.Picasso;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import tv.loqoo.FourthScreen.Utils.AnimationRepeater;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragCenterLogo extends Fragment {

	String TAG = "loqootv";
	String KEY_URL = "centerLogoUrl";
	String url;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragcenterlogo,
        container, false);
    
    ImageView logo = (ImageView) view.findViewById(R.id.frag_centerLogo);
    Bundle arguments = getArguments();
    url = arguments.getString(KEY_URL);
    Log.v(TAG+"centerLogoUrl", url);
    Picasso.with(getActivity()).load(url).fit().into(logo);
    //Animation pulse = AnimationUtils.loadAnimation(this.getActivity(), R.anim.pulse);
    //new AnimationRepeater(logo, pulse).start();
    return view;
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) { 
    super.onActivityCreated(savedInstanceState);
    
    
  
  //ImageView centerLogo = (ImageView) getActivity().findViewById(R.id.frag_centerLogo);
  //Animation pulse = AnimationUtils.loadAnimation(this.getActivity(), R.anim.pulse);
  //centerLogo.startAnimation(pulse);
  

  
  }

} 