package tv.loqoo.FourthScreen;

import java.io.BufferedInputStream;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Message extends Activity {
	
	//Button downBtn;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message);
        Log.d("LifeCycle.Message","ONcreate");

		Intent intent = getIntent();
		String sceneUrl = intent.getStringExtra("url");
		String networkName = intent.getStringExtra("networkName");
		String networkAvatar = intent.getStringExtra("networkAvatar");
		String sceneDescp = intent.getStringExtra("sceneDescp");
		String sceneTag1 = intent.getStringExtra("sceneTag1");
		String sceneTag3 = intent.getStringExtra("sceneTag3");
		
    }
    
    @Override
    protected void onStart(){
    	super.onStart();
    	Log.d("LifeCycle.Message","ONstart");

    	}

    @Override
    protected void onResume(){
    super.onPause();
    Log.d("LifeCycle.Message","ONresume");
    }

    @Override
    protected void onPause(){
    super.onPause();
    Log.d("LifeCycle.Message","ONpause");
    }

    @Override
    protected void onStop(){
    super.onStop();
    Log.d("LifeCycle.Message","ONstop");
    }
    
    @Override
    protected void onRestart(){
    super.onStop();
    Log.d("LifeCycle.Message","ONrestart");
    }

    @Override
    protected void onDestroy(){
    super.onStop();
    Log.d("LifeCycle.Message","ONdestroy");
    }
    
	 private void customToast(String message, String avatar) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
		 Picasso.with(this).load(avatar).into(imageToast);
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM|Gravity.RIGHT,100,50);
	 	 toast.show();
	 }
    
}