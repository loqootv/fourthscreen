package tv.loqoo.FourthScreen;

import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


public class Video extends Activity {
	
	VideoView video;
	String networkAvatar;
	SceneState sceneState = new SceneState();
	private static Intent intent;

	public static Intent instance() {
	             return intent;
	   }
	

@Override
	protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d("LifeCycle.Video","ONcreate");
    setContentView(R.layout.video);
    video = (VideoView) findViewById(R.id.videoView);
	Intent intent = getIntent();
	String sceneUrl = intent.getStringExtra("url");
	String networkName = intent.getStringExtra("networkName");
	String networkAvatar = intent.getStringExtra("networkAvatar");
	String sceneDescp = intent.getStringExtra("sceneDescp");
	String sceneTag1 = intent.getStringExtra("sceneTag1");
	String sceneTag3 = intent.getStringExtra("sceneTag3");
	Log.d("LifeCycle.Video","FirstIntent");
    new video().execute(sceneUrl);
                     
         
}

@Override
protected void onNewIntent(Intent intent) 
{
    super.onNewIntent(intent);
    Log.d("LifeCycle.Video","ONnewintent");
    if(Video.instance().getStringExtra("methodName").equals("playNew"))
    {
       Log.d("LifeCycle.Video","newIntent");
       playVideo();
    }
    
}


protected void onStart(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.d("LifeCycle.Video","ONstart");
	intent = null;

	}

@Override
protected void onResume(){
super.onPause();
Log.d("LifeCycle.Video","ONresume");
intent = null;
}

@Override
protected void onPause(){
super.onPause();
Log.d("LifeCycle.Video","ONpause");
intent = null;
}

@Override
protected void onStop(){
super.onStop();
Log.d("LifeCycle.Video","ONstop");
intent = null;
}

@Override
protected void onRestart(){
super.onStop();
Log.d("LifeCycle.Video","ONrestart");
intent = null;
}

@Override
protected void onDestroy(){
super.onStop();
Log.d("LifeCycle.Video","ONdestroy");
}






//http://oodlestechnologies.com/blogs/Working-with-MediaController,--VideoView-in-Android#sthash.Aqn3QygP.dpuf

class video extends AsyncTask<String, Uri, Void> {
    Integer track = 0;
    ProgressDialog dialog;
    MediaController media;

    protected void onPreExecute() {
        dialog = new ProgressDialog(Video.this);
        dialog.setMessage("Loading, Please Wait...");
        dialog.setCancelable(true);
        dialog.show();
    }

    protected void onProgressUpdate(final Uri... uri) {

        try {

            media = new MediaController(Video.this);
            video.setMediaController(media);
            media.setPrevNextListeners(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // next button clicked

                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });
            media.show(10000);

            video.setVideoURI(uri[0]);
            video.requestFocus();
            video.setOnPreparedListener(new OnPreparedListener() {

                public void onPrepared(MediaPlayer arg0) {
            		customToast("test", networkAvatar);
            		customToast("test", networkAvatar);
            		customToast("test", networkAvatar);
            		customToast("test", networkAvatar);
            		sceneState.videoIsPlaying(Video.this);
            	    int videoWidth = video.getWidth();
            	    int videoHeight = video.getHeight();
                    video.start();
                    dialog.dismiss();
                }
            });
            
            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
          	  @Override
          	  public void onCompletion(MediaPlayer mp) {
          		  
          		  	//letNetworkWhoSentTheSceneKnowImWatching();
          		  	//letOtherPplWhoRWatchingKnowImWatchingToo();
          		    sceneState.videoIsNotPlaying(Video.this);
                	//Intent welcome = new Intent(Video.this,TV.class);
                    //Video.this.startActivity(welcome);
                    Video.this.finish();

                    
          	    
          	  }
          	});
             

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         

    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            Uri uri = Uri.parse(params[0]);
             
            publishProgress(uri);
        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }
    
    

     
}

private void customToast(String message, String avatar) {
	 LayoutInflater li = getLayoutInflater();
	 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
			 (R.id.toastLayout));
	 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
	 Picasso.with(this).load(avatar).into(imageToast);
	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 text.setText(message);
	 Toast toast = new Toast(this);
	 toast.setDuration(Toast.LENGTH_LONG);
	 toast.setView(toastLayout);
	 toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM|Gravity.RIGHT,100,50);
	 toast.show();
}


public void playVideo() {
	intent = Video.instance();
	String sceneUrl = intent.getStringExtra("url");
	String networkName = intent.getStringExtra("networkName");
	String networkAvatar = intent.getStringExtra("networkAvatar");
	String sceneDescp = intent.getStringExtra("sceneDescp");
	String sceneTag1 = intent.getStringExtra("sceneTag1");
	String sceneTag3 = intent.getStringExtra("sceneTag3");
	
    new video().execute(sceneUrl);
}




}
