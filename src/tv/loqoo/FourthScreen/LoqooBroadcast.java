package tv.loqoo.FourthScreen;

import tv.loqoo.FourthScreen.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONTokener;

import com.squareup.picasso.Picasso;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.net.*;

public class LoqooBroadcast extends BroadcastReceiver {

	private static final String TAG = "LoqootvBroadcast";
	String test;
	String xincomingScene;
	String output;
	String Tag1;
	String Tag3;
	String sceneLat;
	String sceneLong;
	String networkName;
	String channelName;
    String networkAvatar;
    String message;
    String networkPassword;
    String sceneLikes;
	String sceneCreatorWebpage;
	String sceneTitle;
	String sceneDesc;
	String sceneCreatorProfileUrl;
	String sceneViews;
	String scenePurchaseLink;
	String sceneImageUrl;
	String sceneCreatorProfileImageUrl;
	String sceneProvider;
    String sceneDuration;
    String sceneDislikes;
    String sceneYTChannelName;
    String sceneCreatorAvatar;
    String sceneCreatorFollowers;
    Intent scene;
    public static final String CLOSE_Activity = "tv.loqoo.v4.closeactivity";
	//0=video,1=message,2=music,3=image
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.i("start", action);
			if(action.equalsIgnoreCase("tv.loqoo.v4.YOYOYO")){
				runIt(context, intent);
			}else if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE")){
				processIncomingScene(context, 0, "video", intent, action );
            }else if(action.equalsIgnoreCase("tv.loqoo.ORIGINAL_AUDIO_SCENE")){
            	processIncomingScene(context, 2, "audio", intent, action);
            }else if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_IMAGE_SCENE")){
            	processIncomingScene(context, 3, "image", intent, action);
            }else if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_MESSAGE_SCENE")){
            	processIncomingScene(context, 3, "message", intent, action);				  
            }else if(action.equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_YOUTUBE_SCENE")){
            	processIncomingScene(context, 77, "youtubeNATIVE", intent, action);
            }else if(action.equalsIgnoreCase("tv.loqoo.v4.ON_SCREEN_CHAT")){
            	processIncomingScene(context, 88, "chat", intent, action);
            }else if(action.equalsIgnoreCase(CLOSE_Activity)){
            	System.out.println("HIT OUTGOING");      
            	Intent i = new Intent();      
            	i.setAction(CLOSE_Activity);       
            	context.sendBroadcast(i);
            }
            			  	    
            			  	  
}
		
		void checkIfSceneIsPlaying(Context context, String action, Intent intent) {
			SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
			Boolean videoplaying = (sharedPrefs.getBoolean("videoIsPlaying", false));
			Boolean musicplaying = (sharedPrefs.getBoolean("musicIsPlaying", false));
			
			if(videoplaying!=true && musicplaying!=true){
				if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE")){
					processIncomingScene(context, 0, "video", intent, action);
				}else if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_AUDIO_SCENE")){
					processIncomingScene(context, 2, "music", intent, action);
				}else if(action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_IMAGE_SCENE")){
					processIncomingScene(context, 3, "image", intent, action);
				};
				
			}
			
			if(videoplaying==true && musicplaying!=true || videoplaying!=true && musicplaying==true){
				cacheScene(context);
			}

			
		}
		
		
		void cacheScene(Context context) {
			SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
			SharedPreferences.Editor editor = sharedPrefs.edit();

		}
		
		 private void customToast(Context context, String message, String avatar) {
			 LayoutInflater li = LayoutInflater.from(context);
			 View toastLayout = li.inflate(R.layout.toast,null);
			 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
			 Picasso.with(context).load(avatar).into(imageToast);
		 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
		 	 text.setText(message);
		 	 Toast toast = new Toast(context);
		 	 toast.setDuration(Toast.LENGTH_LONG);
		 	 toast.setView(toastLayout);
		 	 toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM|Gravity.RIGHT,100,50);
		 	 toast.show();
		 }
		 
		 public void processIncomingScene(Context context, int type, String sceneType, Intent intent, String action ){
				try {
					Log.d("broadcasting", "broadcasting...."+type);
					JSONObject message = null;
					message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
					String incomingScene = message.getString("url");
					Log.d("broadcasting", incomingScene);
					output = incomingScene.replaceFirst("s","");
					sceneProvider = message.getString("sceneProvider");
					Log.d("LoqooBroadcast.Provider",sceneProvider);
					if(sceneProvider.equalsIgnoreCase("loqootv")){
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log", sceneTitle);
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
			            Log.d("LoqooBroadcast.Long", sceneLong);
					}else if(sceneProvider.equalsIgnoreCase("youtubeScene")){
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log",sceneTitle ); 
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
			            sceneDuration = message.getString("sceneDuration");
			            sceneViews = message.getString("sceneViews");
			            sceneLikes = message.getString("sceneLikes");
			            sceneDislikes = message.getString("sceneDisLikes");
			            sceneDesc = message.getString("sceneDesc");   
			            sceneYTChannelName = message.getString("sceneYTChannelName");
			            sceneCreatorAvatar = message.getString("sceneCreatorAvatar");
			            sceneCreatorFollowers = message.getString("sceneCreatorFollowers");
			            sceneCreatorProfileUrl = message.getString("sceneCreatorProfileUrl");
			            sceneImageUrl = message.getString("sceneImageUrl");
					}else if(sceneProvider.equalsIgnoreCase("facebookScene")){
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log", sceneTitle);
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
						
					}else if(sceneProvider.equalsIgnoreCase("pinterestScene")){
						
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log", sceneTitle);
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
					}else if(sceneProvider.equalsIgnoreCase("instagramScene")) {
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log", sceneTitle);
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
					}else if(sceneProvider.equalsIgnoreCase("vineScene")) {
						networkName = message.getString("networkName");
						sceneTitle = message.getString("sceneTitle");
			            networkAvatar = message.getString("networkAvatar");
			            Log.d("IncomingScene.Log", sceneTitle);
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");
			
					}else if(sceneProvider.equalsIgnoreCase("soundcloudScene")) {
						sceneTitle = message.getString("sceneTitle");
						networkName = message.getString("networkName");
						sceneLikes = message.getString("sceneLikes");
						sceneCreatorWebpage = message.getString("sceneCreatorWebpage");
						sceneTitle = message.getString("sceneTitle");
						sceneDesc = message.getString("sceneDesc");
						sceneCreatorProfileUrl = message.getString("sceneCreatorProfileUrl");
						sceneViews = message.getString("sceneViews");
						scenePurchaseLink = message.getString("scenePurchaseLink");
						sceneImageUrl = message.getString("sceneImageUrl");
						sceneCreatorProfileImageUrl = message.getString("sceneCreatorProfileImageUrl");
			            networkAvatar = message.getString("networkAvatar");
			            Tag1 = message.getString("sceneTag1");
			            Tag3 = message.getString("sceneTag3");
			            sceneLat = message.getString("sceneLat");
			            sceneLong = message.getString("sceneLong");	
					}
		            Log.d("LoqooBroadcast.incomingScene",incomingScene);
		            Log.d("LoqooBroadcast.incomingScene",output);
		            SharedPreferences sharedPrefs = context.getSharedPreferences("scenes", 0);
		            SharedPreferences.Editor editor = sharedPrefs.edit();
		            editor.putString("myLastIncomingSceneUrl", output);
		            Log.d("LoqooBroadcast.sharedPref",output);
		            editor.commit();
		            //tools.FlurryEventSCENES(networkName, channelName, output, Tag1, Tag3, sceneType );
		            //checkIfSceneIsPlaying(context, action, intent);
		            switch(type) {
		            case 0:
		            	scene = new Intent( context, Video.class);
		                break;
		            case 1:
		            	scene = new Intent( context, Message.class);
		                break;
		            case 2:
		            	scene = new Intent( context, Music.class);
		                break;
		            case 3:
		            	scene = new Intent( context, Image.class);
		                break;
		            			}
					if(sceneProvider.equalsIgnoreCase("loqootv")){
					scene.putExtra("sceneProvider", sceneProvider);
					scene.putExtra("url", output);
					scene.putExtra("sceneTitle", sceneTitle);
		            scene.putExtra("networkAvatar", networkAvatar);
		            scene.putExtra("networkName", networkName);
		            scene.putExtra("Tag1", Tag1);
		            scene.putExtra("Tag3", Tag3);
		            scene.putExtra("sceneLat", sceneLat);
		            scene.putExtra("sceneLong", sceneLong);
		            scene.putExtra("methodName", "playNew");
					}else if(sceneProvider.equalsIgnoreCase("soundcloudScene")){
						scene.putExtra("sceneProvider", sceneProvider);
			    		scene.putExtra("url", output);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneCreatorWebpage", sceneCreatorWebpage);
			            scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("sceneDesc", sceneDesc);
			            scene.putExtra("sceneCreatorProfileUrl", sceneCreatorProfileUrl);
			            scene.putExtra("sceneViews", sceneViews);
			            scene.putExtra("scenePurchaseLink", scenePurchaseLink);
			            scene.putExtra("sceneImageUrl", sceneImageUrl);
			            scene.putExtra("sceneCreatorProfileImageUrl", sceneCreatorProfileImageUrl);
					}else if(sceneProvider.equalsIgnoreCase("youtubeScene")){
						scene.putExtra("sceneProvider", sceneProvider);
						scene.putExtra("url", output);
						scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneLat", sceneLat);
			            scene.putExtra("sceneLong", sceneLong);
			            scene.putExtra("sceneDuration", sceneDuration);
			            scene.putExtra("sceneCreatorProfileUrl", sceneCreatorProfileUrl);
			            scene.putExtra("sceneCreatorFollowers", sceneCreatorFollowers);
			            scene.putExtra("sceneCreatorAvatar", sceneCreatorAvatar);
			            scene.putExtra("sceneYTChannelName", sceneYTChannelName);
			            scene.putExtra("sceneDesc", sceneDesc);
			            scene.putExtra("sceneLikes", sceneLikes);
			            scene.putExtra("sceneDisLikes", sceneDislikes);
			            scene.putExtra("sceneViews", sceneViews);
			            scene.putExtra("sceneImageUrl", sceneImageUrl);
						
					}else if(sceneProvider.equalsIgnoreCase("vineScene")){
						scene.putExtra("sceneProvider", sceneProvider);
						scene.putExtra("url", output);
						scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneLat", sceneLat);
			            scene.putExtra("sceneLong", sceneLong);
					}else if(sceneProvider.equalsIgnoreCase("instagramScene")){
						scene.putExtra("sceneProvider", sceneProvider);
						scene.putExtra("url", output);
						scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneLat", sceneLat);
			            scene.putExtra("sceneLong", sceneLong);
						
					}else if(sceneProvider.equalsIgnoreCase("facebookScene")){
						scene.putExtra("sceneProvider", sceneProvider);
						scene.putExtra("url", output);
						scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneLat", sceneLat);
			            scene.putExtra("sceneLong", sceneLong);
						
					}else if(sceneProvider.equalsIgnoreCase("pinterestScene")){
						scene.putExtra("sceneProvider", sceneProvider);
						scene.putExtra("url", output);
						scene.putExtra("sceneTitle", sceneTitle);
			            scene.putExtra("networkAvatar", networkAvatar);
			            scene.putExtra("networkName", networkName);
			            scene.putExtra("Tag1", Tag1);
			            scene.putExtra("Tag3", Tag3);
			            scene.putExtra("sceneLat", sceneLat);
			            scene.putExtra("sceneLong", sceneLong);
					}
		            scene.addFlags(Intent.FLAG_FROM_BACKGROUND);
		            scene.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); 
		            scene.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		            context.startActivity(scene);
		            }catch(JSONException e){
		            	System.out.printf("fScreen.IncomingScene.Error", e);
		            	
		            }
		 }
		 
		 public void runIt(Context context, Intent intent){
				JSONObject message = null;
	            try {
	                message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
	                if(message != null){
	                	String pic = message.getString("profilepic");
	                	String msg = message.getString("data");
	                    customToast(context, msg, pic);
	                }
	            } catch (JSONException e) {
	                e.printStackTrace();
	            }
			}
		 
		
}