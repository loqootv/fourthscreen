package tv.loqoo.FourthScreen;

import tv.loqoo.FourthScreen.R;
import static com.squareup.picasso.Callback.EmptyCallback;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Image extends Activity {
	
	ProgressBar progressBar;
	ImageView image;
	ImageView av;
	String sceneUrl;
	String networkAvatar;
	public static final String CLOSE_Activity = "tv.loqoo.v4.closeactivity";
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("LifeCycle.Image","ONcreate");
		setContentView(R.layout.image);
		Intent intent = getIntent();
		sceneUrl = intent.getStringExtra("url");
		Log.d("Image.incomingscene.url",sceneUrl);
		networkAvatar = intent.getStringExtra("networkAvatar");
		Log.d("Image.incomingSceneAvatar",networkAvatar);
		image = (ImageView)findViewById(R.id.imageView1);
		image.setScaleType(ScaleType.FIT_CENTER);
		progressBar = (ProgressBar)findViewById(R.id.progressBar1);
		Picasso.with(this).load(sceneUrl).skipMemoryCache().fit().into(image, new EmptyCallback() {
			
		});
		Log.d("Image.incomingScenePicasso",sceneUrl);
		//av = (ImageView)findViewById(R.id.im);
		//Picasso.with(this).load(networkAvatar).into(av);
		
		String networkName = intent.getStringExtra("networkName");
		String sceneDescp = intent.getStringExtra("sceneDescp");
		String sceneTag1 = intent.getStringExtra("sceneTag1");
		String sceneTag3 = intent.getStringExtra("sceneTag3");
		customToast("test", networkAvatar);
		customToast("test", networkAvatar);
		customToast("test", networkAvatar);
		customToast("test", networkAvatar);
		

		
		
	}
	
	protected void onStart(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("LifeCycle.Image","ONstart");

		}
	
	@Override
	protected void onPause(){
	super.onPause();
	finish();
    
	Picasso.with(this).cancelRequest(image);

	}
	

	@Override
	protected void onResume(){
	super.onPause();
	Log.d("LifeCycle.Image","ONresume");
	Log.d("loqooBroadcastInside.ONRESuME","register_receiever");
	IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("KILL");
	registerReceiver(loqooBroadcastInside, intentFilter);

	}

	@Override
	protected void onStop(){
	super.onStop();
	Log.d("LifeCycle.Image","ONstop");
	}
	
	@Override
	protected void onRestart(){
	super.onStop();
	Log.d("LifeCycle.Image","ONrestart");
	}

	@Override
	protected void onDestroy(){
	super.onStop();
	Log.d("LifeCycle.Image","ONdestroy");
	}
	
	
	 private void customToast(String message, String avatar) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
		 Picasso.with(this).load(avatar).into(imageToast);
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM|Gravity.RIGHT,100,50);
	 	 toast.show();
	 }
	 
		private BroadcastReceiver loqooBroadcastInside = new BroadcastReceiver() {
		    @Override
		    public void onReceive(Context context, Intent intent) {
		    	Log.d("loqooBroadcastInside","start_onRecieve_Insideactivity");
		        if (intent.getAction().equals(LoqooBroadcast.CLOSE_Activity)) {
		        	Log.d("loqooBroadcastInside","finish()insideactivity");
		            finish();
		            unregisterReceiver(loqooBroadcastInside);
		            disable(context);
		        }
		    }
		};
		
		public static void enable(Context context){
			  Log.d("TAG", "Enabled Broadcast Receiver");
			  ComponentName compName = new ComponentName(context, LoqooBroadcast.class);
			  context.getPackageManager().setComponentEnabledSetting(compName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
			 }
		
		public static void disable(Context context){
			  Log.d("TAG", "Disabled Broadcast Receiver");
			  ComponentName compName = new ComponentName(context, LoqooBroadcast.class);
			  context.getPackageManager().setComponentEnabledSetting(compName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);  
			 }
		

		
		
	
}