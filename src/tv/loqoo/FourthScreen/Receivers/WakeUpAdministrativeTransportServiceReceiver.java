package tv.loqoo.FourthScreen.Receivers;

import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WakeUpAdministrativeTransportServiceReceiver extends BroadcastReceiver {
    NotificationManager nm;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, AdministrativeTransportService.class);
        context.startService(myIntent);
    }
}