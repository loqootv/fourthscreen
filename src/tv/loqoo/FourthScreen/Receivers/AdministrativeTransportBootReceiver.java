package tv.loqoo.FourthScreen.Receivers;

import java.util.UUID;

import tv.loqoo.FourthScreen.Utils.DeviceUuidFactory;
import tv.loqoo.FourthScreen.Initialization.Init;
import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import tv.loqoo.FourthScreen.Services.TransportService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class AdministrativeTransportBootReceiver extends BroadcastReceiver {
	
	AlarmManager am;

    @Override
    public void onReceive(Context context, Intent arg1) {
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        setRepeatingAlarm(context);
        Log.i("PubnubService", "AdministrativeTransport BootReceiver Starting");
        //Intent startApp = new Intent(context, Init.class);
        //xcontext.startActivity(startApp);	
        Intent intent = new Intent(context, AdministrativeTransportService.class);
        context.startService(intent);
        Log.i("PubnubService", "PubNub BootReceiver Started");
    }
    
    public void setRepeatingAlarm(Context context) {
        Intent intent = new Intent(context, WakeUpAdministrativeTransportServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                (5 * 60 * 1000), pendingIntent); //wake up every 5 minutes to ensure service stays alive
    }
    
}