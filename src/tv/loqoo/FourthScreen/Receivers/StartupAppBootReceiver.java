package tv.loqoo.FourthScreen.Receivers;

import tv.loqoo.FourthScreen.Initialization.Init;
import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class StartupAppBootReceiver extends BroadcastReceiver {
	
	AlarmManager am;

    @Override
    public void onReceive(Context context, Intent arg1) {
        Log.i("LoQooTV", "LoQooTV is Starting");
        Intent startApp = new Intent(context, Init.class);
        context.startActivity(startApp);
    }
    
    
}