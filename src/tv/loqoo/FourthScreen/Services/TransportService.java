package tv.loqoo.FourthScreen.Services;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Start;
import tv.loqoo.FourthScreen.R.drawable;
import tv.loqoo.FourthScreen.Channels.Channel_0001;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.KeyValueStore;
import tv.loqoo.FourthScreen.Utils.LTVID;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.google.gson.Gson;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubException;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class TransportService extends Service {

	String TAG = "loqootv";
	MessageHandler mMessageHandler = new MessageHandler();
	final static int myID = 1234;
	DeviceInfo dvinfo;
	String sessionId;
	String id = DeviceInfo.tvId(this);
	KeyValueStore kvstore;
    String channel;
    String PUBDEV = "";
    String SUBDEV = "";
	Pubnub pubnub = new Pubnub("pub-c-5ef82c66-594b-44bc-95f4-a27df6342e8e","sub-c-488149b6-ff29-11e3-94a5-02ee2ddab7fe", false);
	String AppPrefs ="LoQooTVPrefs";
	SharedPreferences sp;
	
    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String pnMsg = msg.obj.toString();

            final Toast toast = Toast.makeText(getApplicationContext(), pnMsg, Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 22200);

        }
    };

    private void notifyUser(Object message) {

        Message msg = handler.obtainMessage();

        try {
            final String obj = (String) message;
            msg.obj = obj;
            handler.sendMessage(msg);
            Log.i("Received msg : ", obj.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreate() {
        super.onCreate();
        pubnub.setUUID(id);
        
    }
    
    private void broadcastMessage(JSONObject message) {	
    	String msg = null;
    	try {
		msg = message.getString("action");
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
        Intent intent = new Intent(msg);
        intent.putExtra("message", message.toString());
        sendBroadcast(intent);
        
    }
    
    class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            try {
                String m = msg.getData().getString("message");
                Log.i("handleMessage", m);
                JSONObject message = (JSONObject) new JSONTokener(m).nextValue();
                broadcastMessage(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    };
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
    	if(channel == null){
    		channel = intent.getStringExtra("channelName");
    		Log.d("onStartCommand.ChannelTransport", channel);
    	}else {
    		channel = "@loqootv";
    		Log.d("onStartCommand.ChannelTransport", channel);
    	}
    	SharedPreferences sp = getSharedPreferences(AppPrefs, TransportService.this.MODE_PRIVATE);
    	sessionId = sp.getString(sessionId, "default_no");
    	Startpubnub(channel,sessionId);
    	Log.d("TV.channelTransport.onStartCommand", channel);
    	
    	return Service.START_REDELIVER_INTENT;
    }
    
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, channel+"TransportLine deactivated...", Toast.LENGTH_LONG).show();
        Log.i("PUBNUB", channel+"TransportLine deactivated...");
    }	

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
    
    public void Startpubnub(String xchannel, String xsessionId) {
    Toast.makeText(this, channel+"TransportLine activated...", Toast.LENGTH_LONG).show();
    Log.i("PUBNUB", channel+"TransportLine activated...");
    try {
        pubnub.subscribe(new String[]{xchannel,xsessionId,"LOQOOTVISTHENEWTV"}, new Callback() {
            public void connectCallback(String channelName) {
            	notifyUser("CONNECT on channel:" + channelName);
            }
            public void disconnectCallback(String channelName) {
                notifyUser("DISCONNECT on channel:" + channelName);
            }
            public void reconnectCallback(String channelName) {
                notifyUser("RECONNECT on channel:" + channelName);
            }
            @Override
            public void successCallback(String channelName, Object message) {
            	Log.i("tag","broadcast is sent!");
                notifyUser(channelName + " " + message.toString());
                Log.i("afterBroadcastisSent", message.toString());
                try {
                    Message m = Message.obtain();
                    Bundle b = new Bundle();
                    b.putString("message", message.toString());
                    m.setData(b);
                    mMessageHandler.sendMessage(m);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void errorCallback(String channelName, Object message) {
                notifyUser(channelName + " " + message.toString());
            }
        });
    } catch (PubnubException e) {

    }
    
    Notification note=new Notification(R.drawable.ltvicon,"Can you hear the music?",System.currentTimeMillis());
    		Intent i=new Intent(this, Channel_0001.class);
    		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|
    				Intent.FLAG_ACTIVITY_SINGLE_TOP);
    		PendingIntent pi=PendingIntent.getActivity(this, 0,i, 0);
    		note.setLatestEventInfo(this, "Fake Player","Now Playing: \"Ummmm, Nothing\"",pi);
    		note.flags|=Notification.FLAG_NO_CLEAR;
    		startForeground(myID, note);

    }
}