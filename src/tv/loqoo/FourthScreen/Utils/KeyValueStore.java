package tv.loqoo.FourthScreen.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


public class KeyValueStore {

    private static final String USER_PREFS = "USER_PREFS";
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
    private String subscribed = "subscribed_prefs";
    private String remoteid = "remoteid_prefs";
    private String currentChannel = "currentChannel";
    private Boolean isSubscribed = false;


    public KeyValueStore(Context context) {
        this.sharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.editor = sharedPrefs.edit();
    }

    public void setDataInt(String data, Integer dataInt){
        editor.putInt(data, dataInt);
    }

    public Integer getDataInt(String data, Integer dataInt){
        return sharedPrefs.getInt(data, dataInt );
    }

    public void setData(String data){
        editor.putString(data, data).commit();
    }

    public String getData(String data){
        return sharedPrefs.getString(data,"");
    }

    public void setisData(String data, Boolean isData){
        editor.putBoolean(data, isData).commit();
    }

    public Boolean getisData(String data){
        return sharedPrefs.getBoolean(data,false);
    }

    public void setisSubscribed(Boolean isSubscribed) {
        editor.putBoolean(subscribed, isSubscribed).commit();
    }

    public Boolean getisSubscribed() {
        return sharedPrefs.getBoolean(subscribed, false);
    }


    public String getTvId() {
        return sharedPrefs.getString(remoteid,"");
    }

    public void setTvId(String remoteid) {
        editor.putString(remoteid,remoteid).commit();
    }
    
    public void setCurrentChannel(String channel){
    	editor.putString(currentChannel, channel);
    }
    
    public String getCurrentChannel(){
    	return sharedPrefs.getString(currentChannel, "@loqootv");
    }

}
    