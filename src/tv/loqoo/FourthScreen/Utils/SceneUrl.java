package tv.loqoo.FourthScreen.Utils;

public class SceneUrl {
	
	public static String networkName;
	public static String networkEmail;
	public static String sceneTag1;
	public static String sceneTip;
	public static String sceneTag3;
	public static String channelName;
	public static String sceneTipDnom;
	public static String sceneType;
	public static String networkAvatar;
	public static String s3Location;
	public static String contentUrl;
	public static String xphoneNumber;
	public static String xltvID;
	public static String sessionId;

	public String originalSceneUrl(String eventType, String networkName, String networkEmail, String channelName,
			String sceneType, String s3Location,String sceneTag1, String sceneTag3,String sceneTip, String sceneTipDnom, String networkAvatar ) 
	{
		 return "http://loqootv.com/log?eventType="+eventType+"&networkName="+networkName+"&networkEmail="+networkEmail+"&sendToChannel="+channelName+
		"&sceneType="+sceneType+"&sceneUrl="+s3Location+"&sceneTag1="+sceneTag1+"&sceneTag3="+sceneTag3+"&sceneTip="+sceneTip+"&sceneTipDnom="+sceneTipDnom+"&networkAvatar="+networkAvatar+"&timestamp="+System.currentTimeMillis();
	}
	
	public String thirdPartySceneUrl(String eventType, String networkName, String networkEmail, String channelName,
			String sceneType, String contentUrl,String sceneTag1, String sceneTag3,String sceneTip, String sceneTipDnom, String networkAvatar )
	{
		 return "http://loqootv.com/log?eventType="+eventType+"&networkName="+networkName+"&networkEmail="+networkEmail+"&sendToChannel="+channelName+
		"&sceneType="+sceneType+"&sceneUrl="+contentUrl+"&sceneTag1="+sceneTag1+"&sceneTag3="+sceneTag3+"&sceneTip="+sceneTip+"&sceneTipDnom="+sceneTipDnom+"&networkAvatar="+networkAvatar+"&timestamp="+System.currentTimeMillis();
	}
	
	public String pinterestSceneUrl(String eventType, String networkName, String networkEmail, String channelName,
			String sceneType, String sceneurl,String sceneTag1, String sceneTag3,String sceneTip, String sceneTipDnom, String networkAvatar,
			String Message, String pinterestID )
	{
		 return "http://loqootv.com/log?eventType="+eventType+"&networkName="+networkName+"&networkEmail="+networkEmail+"&sendToChannel="+channelName+
		"&sceneType="+sceneType+"&sceneUrl="+s3Location+"&sceneTag1="+sceneTag1+"&sceneTag3="+sceneTag3+"&sceneTip="+sceneTip+"&sceneTipDnom="+sceneTipDnom+"&networkAvatar="+networkAvatar+"&timestamp="+System.currentTimeMillis();
	}
	
	public String gMessage(String eventType, String networkName)
	{
		 return "http://loqootv.com/log?eventType="+eventType+"&networkName="+networkName+"&timestamp="+System.currentTimeMillis();
	}
	
	public String jsonString(String phoneNumber, String ltvID, long timestamp, String eventType ){
		
		return 	"{\"phoneNumber\":\""+xphoneNumber+"\",\"ltvID\":\""+xltvID+"\",\"timestamp\":\""+timestamp+"\",\"eventType\":\""+eventType+"\"}";
	}
	
	public String phoneNumberString(String phoneNumber, String ltvID, long timestamp, String eventType, String sessionID ){
		
		return 	"{\"phoneNumber\":\""+xphoneNumber+"\",\"ltvID\":\""+xltvID+"\",\"timestamp\":\""+timestamp+"\",\"eventType\":\""+eventType+"\",\"sessionId\":\""+sessionId+"\"}";
	}
}
