package tv.loqoo.FourthScreen.Utils;

import java.util.UUID;

import tv.loqoo.FourthScreen.Utils.DeviceUuidFactory;
import android.content.Context;

public class LTVID {
	
	protected Context context;
	public String ltvPrivateIDString;
	public String ltvPublicIDString;
	
	public String privateLTVID(Context context){
		UUID ltvID = new DeviceUuidFactory(context).getDeviceUuid();
		ltvPrivateIDString = String.valueOf(ltvID).replaceAll("-", "");
		
		return ltvPrivateIDString;
	}
	
	public String publicLTVID(){
		
		ltvPublicIDString = new RandomString(12).nextString();
		return ltvPublicIDString;
		
	}
	
	
}