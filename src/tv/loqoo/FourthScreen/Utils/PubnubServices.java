package tv.loqoo.FourthScreen.Utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.pubnub.api.Callback;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONObject;

/**
 * Created by creator on 8/12/14.
 */
public class PubnubServices {
	
	private Context mContext;
    String TAG = "loqootv";
    com.pubnub.api.Pubnub pubnub = new com.pubnub.api.Pubnub("pub-c-5ef82c66-594b-44bc-95f4-a27df6342e8e",
            "sub-c-488149b6-ff29-11e3-94a5-02ee2ddab7fe");
    
    public PubnubServices(Context context){
        mContext = context;
    }
        

    public void receivemsg(String channel) {
        Toast.makeText(mContext, "RemoteTransportSession activated.....", Toast.LENGTH_LONG).show();
        Log.i("PUBNUB", "RemoteTransportSession activated activate...");
        try {
            pubnub.subscribe(channel, new Callback() {

                        @Override
                        public void connectCallback(String channel, Object message) {
                            Log.d(TAG+"PUBNUBconnect", "SUBSCRIBE : CONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                            pubnub.unsubscribe(channel);
                        }

                        @Override
                        public void disconnectCallback(String channel, Object message) {
                            Log.d(TAG+"PUBNUBdisconnect", "SUBSCRIBE : DISCONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                        }

                        public void reconnectCallback(String channel, Object message) {
                            Log.d(TAG+"PUBNUBreconnect", "SUBSCRIBE : RECONNECT on channel:" + channel
                                    + " : " + message.getClass() + " : "
                                    + message.toString());
                            pubnub.unsubscribe(channel);
                        }

                        @Override
                        public void successCallback(String channel, Object message) {
                            Log.d(TAG+"PUBNUBsubscribeSUCCESS", "SUBSCRIBE : " + channel + " : "
                                    + message.getClass() + " : " + message.toString());
                        }

                        @Override
                        public void errorCallback(String channel, PubnubError error) {
                            Log.d(TAG+"PUBNUBsubscribeERROR", "SUBSCRIBE : ERROR on channel " + channel
                                    + " : " + error.toString());
                        }
                    }
            );
        } catch (PubnubException e) {
            Log.d("PUBNUB", e.toString());
        }
    }

    public void sendmsgJSON(String channel, JSONObject message) {
            Callback callback = new Callback() {
                public void successCallback(String channel, Object response) {
                    Log.d(TAG+"PUBNUBsentmessageSUCCESS",response.toString());
                }
                public void errorCallback(String channel, PubnubError error) {
                    Log.d(TAG+"PUBNUBsentmessageERROR",error.toString());
                }
            };
            pubnub.publish(channel, message , callback);
            pubnub.unsubscribe(channel);
    }

    public void sendmsgString (String channel, String message1) {
        Callback callback = new Callback() {
            public void successCallback(String channel, Object response) {
                Log.d(TAG+"PUBNUBsentmessageSUCCESS",response.toString());
            }
            public void errorCallback(String channel, PubnubError error) {
                Log.d(TAG+"PUBNUBsentmessageERROR",error.toString());
            }
        };
        pubnub.publish(channel, message1 , callback);
        pubnub.unsubscribe(channel);
    }


}