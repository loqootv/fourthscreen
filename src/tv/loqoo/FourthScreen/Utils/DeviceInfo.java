package tv.loqoo.FourthScreen.Utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Patterns;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pubnub.api.Pubnub;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

public class DeviceInfo {
    static String TAG = "loqootv";
    String fullname;
    private Context mContext;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    Pattern emailPattern;
    static com.pubnub.api.Pubnub pubnub = new com.pubnub.api.Pubnub("pub-c-5ef82c66-594b-44bc-95f4-a27df6342e8e",
            "sub-c-488149b6-ff29-11e3-94a5-02ee2ddab7fe");

    public DeviceInfo(Context context){
        mContext = context;
    }

    public static String utcDate() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("Etc/Utc"));
        String text = format.format(new Date());
        return text;
    }
    

    public static String utcOffset() {

        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        int offsetFromUtc = tz.getOffset(now.getTime()) / 1000;
        return Integer.toString(offsetFromUtc);
    }
    
    

    public static String fullname(Context context){
        String[] columnNames = new String[] {ContactsContract.Profile.DISPLAY_NAME, ContactsContract.Profile.PHOTO_ID};
        Cursor c = context.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, columnNames, null, null, null);
        int count = c.getCount();
        boolean b = c.moveToFirst();
        int position = c.getPosition();
        String fullname = null;
        if (count == 1 && position == 0) {
            for (int j = 0; j < columnNames.length; j++) {
                fullname = c.getString(0);
                long photoId = c.getLong(1);
                System.out.println(photoId);

            }
        }
        c.close();

        return fullname;
    }

    public static String emailAddress(Context context){
        String email = null;
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        final ArrayList<String> accountsInfo = new ArrayList<String>();
        final Account[] accounts = AccountManager.get(context).getAccounts();
        for (final Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                email = account.name;
                return email;
            }
        }
    return email;
    }
    
    public static boolean isGPSEnabled(Context context) {
    	try{
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    		if (manager != null) {
            LocationProvider gpsProvider = manager
                    .getProvider(LocationManager.GPS_PROVIDER);
            if (gpsProvider != null) {
                return true;
            }
        }
    	}catch(NullPointerException e){
    		e.printStackTrace();
    	}
        return false;
    }

    public static boolean checkNetworkConnection(Context context) {
        if (null != context) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();

            if (mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static String getSerial() {
        return android.os.Build.SERIAL;
    }

    public static String getMacAddress(Context context) {
        WifiManager wifiMan = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        return wifiInf.getMacAddress();
    }


    public static String getAndroidID(Context context) {
        return android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
    }

    public static int getSdkVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String tvId(Context context) {

        if (getSdkVersion() >= 10) {
            // Works since 10 SDK (2.3).
            return getSerial();
        } else {
            final String macAdr, androidId;

            // MAC_ADRESS
            macAdr = getMacAddress(context);
            // ANDROID_ID
            androidId = getAndroidID(context);

            // Generating of 128-bit universal unique key
            UUID deviceUuid = new UUID(androidId.hashCode(), macAdr.hashCode());

            return deviceUuid.toString().replaceAll("-", "");
        }
    }

    public static boolean isBluetoothAvailable() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) ? true : false;
    }

    public static boolean isBleAvailable(Context ctx) {
        PackageManager pm = ctx.getPackageManager();
        return (pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) ? true  : false;

    }
    public static boolean isWifiConnected(Context context) {

    	NetworkInfo info = DeviceInfo.getNetworkInfo(context);
    	if (info != null && info.isConnected()){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return (mWifi.isConnected()) ? true : false;
    	}
    	return false;
    }

    public static boolean isEthernetConnected(Context context) {
    	NetworkInfo info = DeviceInfo.getNetworkInfo(context);
    	if (info != null && info.isConnected()){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo mEthernet = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        return (mEthernet.isConnected()) ? true : false;
    	}
    	return false;
    }

    public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }


    public static boolean hasEthernetPort(Context context){
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    if(cm.getActiveNetworkInfo().getType()==9){
                            return true;
                    }                
            return false;            
        }           

        /**
         * Check if there is any connectivity
         * @param context
         * @return
         */
        public static boolean isConnected(Context context){
            NetworkInfo info = DeviceInfo.getNetworkInfo(context);
            return (info != null && info.isConnected());
        }

        /**
         * Check if there is any connectivity to a Wifi network
         * @param context
         * @return
         */
        public static boolean isConnectedWifi(Context context){
            NetworkInfo info = DeviceInfo.getNetworkInfo(context);
            return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
        }

        /**
         * Check if there is any connectivity to a mobile network
         * @param context
         * @return
         */
        public static boolean isConnectedMobile(Context context){
            NetworkInfo info = DeviceInfo.getNetworkInfo(context);
            return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
        }

        /**
         * Check if there is fast connectivity
         * @param context
         * @return
         */
        public static boolean isConnectedFast(Context context){
            NetworkInfo info = DeviceInfo.getNetworkInfo(context);
            return (info != null && info.isConnected() && DeviceInfo.isConnectionFast(info.getType(),info.getSubtype()));
        }



        /**
         * Check if the connection is fast
         * @param type
         * @param subType
         * @return
         */
        public static boolean isConnectionFast(int type, int subType){
            if(type==ConnectivityManager.TYPE_WIFI){
                return true;
            }else if(type==ConnectivityManager.TYPE_MOBILE){
                switch(subType){
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                        return false; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                        return false; // ~ 14-64 kbps
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                        return false; // ~ 50-100 kbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        return true; // ~ 400-1000 kbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        return true; // ~ 600-1400 kbps
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                        return false; // ~ 100 kbps
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                        return true; // ~ 2-14 Mbps
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                        return true; // ~ 700-1700 kbps
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                        return true; // ~ 1-23 Mbps
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                        return true; // ~ 400-7000 kbps
			/*
			 * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these
			 */
                    case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                        return true; // ~ 1-2 Mbps
                    case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                        return true; // ~ 5 Mbps
                    case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                        return true; // ~ 10-20 Mbps
                    case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                        return false; // ~25 kbps
                    case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                        return true; // ~ 10+ Mbps
                    // Unknown
                    case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    default:
                        return false;
                }
            }else{
                return false;
            }
        }
        
    public static String publicIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.d("VPNConnected", ip);
                        return ip;
                    }
                }
            }

        } catch (Exception ex) {
            Log.v("exception", ex.toString());
        }
        return "0.0.0.0";
    }

    public static String getLine1Number  (Context context) {
    	try{
        TelephonyManager tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        if (tm !=null) {
            return tm.getLine1Number();
        }else {
            return "None";
        }
    	}catch(NullPointerException e){
    		e.printStackTrace();
    		return "None";
    	}
    }

    public static String privateIpAddress (Context context) {
        WifiManager wm = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }


    public static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            } else {
                Log.i(TAG+"NOPE", "This device is not supported.");
            }
            return false;
        }
        return true;
    }


    public static JSONObject getSubscriberData(Context context){
     JSONObject deviceinfo;
     String df = null;
     boolean wifi = false;
     boolean mobileIsConnected = false;
     boolean ethernet = false;
     boolean connected = false;
     boolean isFast = false;
     boolean hasEthernetPort = false;
     boolean mobile = false;
     String macaddress = null;
     String publicip = null;
     String privateip = null;
     boolean hasPlayServices = checkPlayServices(context);
     boolean bluetooth = isBluetoothAvailable();
     boolean gps = isGPSEnabled(context);
     //boolean ble = isBleAvailable(context);
     String phnumber =  getLine1Number(context);
     String utcDate = utcDate();
     String utcOffset = utcOffset();
     String fullname = null;
     try{
     fullname = fullname(context);
     }catch(Exception e){
    	 e.printStackTrace();
    	 fullname = "None";
     }
     String email = null;
     try{
     email = emailAddress(context);
     }catch(Exception e){
    	 email = "None";
    	 e.printStackTrace();
     }
     int sdkversion = getSdkVersion();
     String androidID = getAndroidID(context);
     String tvid = String.valueOf(tvId(context).replaceAll("-", ""));
     
     if(!checkNetworkConnection(context)){
    	 Log.v(TAG+"nointernet", "nointernet");
     }else{
     wifi = isWifiConnected(context);
     mobileIsConnected = isConnectedMobile(context);
     ethernet = isEthernetConnected(context);
     connected = isConnected(context);
     isFast = isConnectedFast(context);
     hasEthernetPort = hasEthernetPort(context);
     mobile = isConnectedMobile(context);
     //macaddress = getMacAddress(context);
     publicip = publicIpAddress();
     privateip = privateIpAddress(context);
     }
 

       // Iterable<String> xfullname = Splitter.on(" ").split(fullname);
       // List<String> names = Lists.newArrayList(xfullname);
        //String firstname = names.get(0);
       // String lastname = names.get(1);

     //Log all collected device/user info
     Log.v(TAG+"hasPlayServices", Boolean.toString(hasPlayServices));
     Log.v("gps",Boolean.toString(gps));
     //try{
     //Log.v(TAG+"email", email);
    // }catch(Exception e){
    //	 e.printStackTrace();
     //}
     try{
     Log.v(TAG+"fullname", fullname);
     }catch(Exception e){
    	 e.printStackTrace();
     }
     Log.v(TAG+"bluetooth",Boolean.toString(bluetooth));
     //Log.v("ble",Boolean.toString(ble));
     Log.v(TAG+"wifi",Boolean.toString(wifi));
     //Log.v("ethernet",Boolean.toString(ethernet));
     Log.v(TAG+"isConnected",Boolean.toString(connected));
     Log.v(TAG+"isFast",Boolean.toString(isFast));
     Log.v(TAG+"androidid", androidID);
     Log.v(TAG+"remoteid", tvid);
     try{
     Log.v(TAG + "phnumber", phnumber);
     }catch(Exception e){
    	 e.printStackTrace();
     }
     Log.v(TAG+"publicip", publicip);
     Log.v(TAG+"privateip", privateip);
     //Log.v(TAG+macaddress, macaddress);

     //Put all device info in JSON
     deviceinfo = new JSONObject();
     try {
     //deviceinfo.put("email", email);
     deviceinfo.put("phnumber", phnumber);
     deviceinfo.put("gps", gps);
     deviceinfo.put("fullname", fullname);
     //deviceinfo.put("firstname", firstname);
     //deviceinfo.put("lastname", lastname);
     deviceinfo.put("bluetooth", Boolean.toString(bluetooth));
     //deviceinfo.put("ble", ble);
     deviceinfo.put("wifi", Boolean.toString(wifi));
     //deviceinfo.put("ethernet", ethernet);
     deviceinfo.put("isConnected", Boolean.toString(connected));
     deviceinfo.put("isFast", Boolean.toString(isFast));
     deviceinfo.put("androidid", androidID);
     deviceinfo.put("remoteid", tvid);
     deviceinfo.put("publicip", publicip);
     deviceinfo.put("privateip", privateip);
     //deviceinfo.put("macaddress", macaddress);
     deviceinfo.put("date", utcDate);
     deviceinfo.put("utcoffset", utcOffset);
     deviceinfo.put("hasEthernetPort", hasEthernetPort);
     deviceinfo.put("sdkversion", sdkversion);
     deviceinfo.put("pubnubuuid",pubnub.getUUID().toString());
     
     df = deviceinfo.toString(2);
     Log.v("deviceinfoJSONString2", df);
     }catch(JSONException e){
     e.printStackTrace();
     }

    return deviceinfo;
    }

}
