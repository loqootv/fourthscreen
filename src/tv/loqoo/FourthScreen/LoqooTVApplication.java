package tv.loqoo.FourthScreen;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Services.TransportService;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.DeviceUuidFactory;
import tv.loqoo.FourthScreen.Utils.KeyValueStore;
import tv.loqoo.FourthScreen.Utils.PubnubServices;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.http.util.ByteArrayBuffer;

import com.pubnub.api.*;
import org.json.*;

import android.R.string;
import android.app.Application;
import android.app.Activity;
import android.app.DownloadManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.text.format.Time;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


public class LoqooTVApplication extends Application {
	
    public LoqooTVApplication() {
    }
    
	
    String TAG = "loqootv";
    public static final int SPLASH_TRANSITION_INTERVAL = 7000;
    public static final int MAIN_THREAD_DELAY = 10000;
	private static final int TIMEOUT_CONNECTION = 0;
	private static final int TIMEOUT_SOCKET = 0;
	public static Context mContext;
	public String value;
	public String registered;
	public String ltvIDString;
	String tvid;
	String username;
	String phnumber;
	String phnumber2;
	KeyValueStore kvstore;
	DeviceInfo dvinfo;
	SharedPreferences sharedPrefs;
	PubnubServices pubnubServices;

    
    
	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			Class.forName("android.os.AsyncTask");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
       
		mContext = getApplicationContext();
        kvstore = new KeyValueStore(mContext);
        dvinfo = new DeviceInfo(mContext);
		

		Time now = new Time(Time.getCurrentTimezone());
		System.out.printf("now",now);
		TimeZone tz = TimeZone.getDefault();
		System.out.printf("tz", tz);
		String tzID = tz.getID();
		System.out.printf("tzID", tzID);
		Date rightnow = new Date();
		int offsetFromUtc = tz.getOffset(rightnow.getTime()) / 1000;
		System.out.printf("offsetfromutc", offsetFromUtc);
		
        tvid = dvinfo.tvId(mContext).toString();
        Log.v(TAG+"tvid", tvid);
        phnumber = dvinfo.getLine1Number(mContext);
        phnumber2 = "1242151";
        username = userName(phnumber, tvid);
        
	}
	
    
    public static Context getContext() {
        return mContext;
    }
    
    
    public String userName(String phnumber, String xtvid){
    	if(phnumber != "None"){
    		return phnumber;
    	}
     return xtvid;
    }
    
}
