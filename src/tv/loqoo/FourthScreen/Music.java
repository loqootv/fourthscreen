package tv.loqoo.FourthScreen;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import com.squareup.picasso.Picasso;
import tv.loqoo.FourthScreen.R.*;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class Music extends Activity {
	
	MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
	MediaPlayer player;	
	ProgressBar progress;
	DownloadFile test;
	String sceneUrl;
	String networkAvatar;
	String networkName;
	String Tag1;
	String artistName;
	String album;
	String songTitle;
	String songDuration;
	String sceneCreatorWebpage;
	String sceneTitle;
	String sceneDesc;
	String sceneCreatorProfileUrl;
	String sceneViews;
	String scenePurchaseLink;
	String sceneImageUrl;
	String sceneCreatorProfileImageUrl;
	
	SceneState sceneState = new SceneState();
	//Button downBtn;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music);
        Log.d("LifeCycle.Music","ONcreate");
        
		Intent intent = getIntent();
		sceneUrl = intent.getStringExtra("url");
		networkName = intent.getStringExtra("networkName");
		networkAvatar = intent.getStringExtra("networkAvatar");
		sceneCreatorWebpage = intent.getStringExtra("sceneCreatorWebpage");
		sceneTitle = intent.getStringExtra("sceneTitle");
		sceneDesc = intent.getStringExtra("sceneDesc");
		sceneCreatorProfileUrl = intent.getStringExtra("sceneCreatorProfileUrl");
		sceneViews = intent.getStringExtra("sceneViews");
		scenePurchaseLink = intent.getStringExtra("scenePurchaseLink");
		sceneImageUrl = intent.getStringExtra("sceneImageUrl");
		sceneCreatorProfileImageUrl = intent.getStringExtra("sceneCreatorProfileImageUrl");		

        //downBtn = (Button)findViewById(R.id.downloadBtn);
        progress = (ProgressBar)findViewById(R.id.progressBar1);
        test = new DownloadFile();
        progress.setVisibility(View.VISIBLE);
        
       
        
    	Map<String, String> meta = new HashMap<String,String>();
    	 if (Build.VERSION.SDK_INT >= 14)
         	metaRetriever.setDataSource(sceneUrl, new HashMap<String, String>());
    	if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)!=null){ 
    		meta.put("Artist",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
    	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)!=null){ 
    		meta.put("Title",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
    	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)!=null){ 
		meta.put("Duration",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
    	}if (metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM)!=null){ 
    		meta.put("Album",metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
    	}
    	
    	artistName = meta.get("Artist");
        songTitle = meta.get("Title");
        songDuration = meta.get("Duration");
        album = meta.get("Album");
    	
    	
    	
    	new DownloadFile().execute(sceneUrl);
        //getMeta(sceneUrl);
        playMusic(sceneUrl);
        ImageView image = (ImageView) findViewById(R.id.imageView1);
        image.setScaleType(ScaleType.FIT_CENTER);
        Picasso.with(this).load(sceneImageUrl).into(image);
        
        

    	

        
    }
    
    protected void onStart(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	Log.d("LifeCycle.Music","ONstart");

    	}

    @Override
    protected void onResume(){
    super.onPause();
    Log.d("LifeCycle.Music","ONresume");
    }

    @Override
    protected void onPause(){
    super.onPause();
    Log.d("LifeCycle.Music","ONpause");
    }

    @Override
    protected void onStop(){
    super.onStop();
    Log.d("LifeCycle.Music","ONstop");
    }

    @Override
    protected void onRestart(){
    super.onStop();
    Log.d("LifeCycle.Music","ONrestart");
    }
    
    @Override
    protected void onDestroy(){
    super.onStop();
    Log.d("LifeCycle.Music","ONdestroy");
    }
    
    public class DownloadFile extends AsyncTask<String,Void,Void> {

        private Context context;
        public void setContext(Context contextf){
            context = contextf;
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                //c.setDoOutput(true);
                c.connect();

                File sdcard = Environment.getExternalStorageDirectory();
                File myDir = new File(sdcard,"Android/data/tv.loqoo.FourthScreen/temp");
                myDir.mkdirs();
                File outputFile = new File(myDir, "music.mp3");
                if(outputFile.exists()){
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.flush();
                fos.close();
                is.close();


            } catch (Exception e) {
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            return null;
        }

   	}
    
    
    private Bitmap downloadBitmap(final String url) {
        metaRetriever.setDataSource(url, new HashMap<String, String>());
        try {
           final byte[] art = metaRetriever.getEmbeddedPicture();
           metaRetriever.release();
           return BitmapFactory.decodeByteArray(art, 0, art.length);
        } catch (Exception e) {
           Log.e("LOGTAG", "Couldn't create album art: " + e.getMessage());
           return BitmapFactory.decodeResource(getResources(), R.drawable.ltvrobots);
           
        }
     }
    
    

    	
  
    public void playMusic(final String sceneUrl) {
    	try {
    		player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(sceneUrl);
            player.setLooping(false);
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {            
                
                @Override
                public void onPrepared(MediaPlayer mp) {
                	progress.setVisibility(View.INVISIBLE);
                	//TODO playing();
        			//LogToQueuewChannel("A Scene You Aired on #"+channelName" Now Playing on"+,networkName, "sceneIsNowPlaying")
                	
                    mp.start();
                    sceneState.videoIsPlaying(Music.this);
            		customToast(sceneDesc, sceneTitle, album,sceneCreatorProfileImageUrl);
            		customToast(sceneDesc, sceneTitle, album,sceneCreatorProfileImageUrl);
                	customToastLogo();
                	customToastLogo();
                	customToast(sceneDesc, sceneTitle, album,sceneCreatorProfileImageUrl);
                	customToast(sceneDesc, sceneTitle, album,sceneCreatorProfileImageUrl);
                }
            });
            
            player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                	Log.e("MEDIAPLAYER ERRORS",
                            "what: " + what + "  extra: "   + extra);
                	mp.reset();
                	try {
						mp.setDataSource(sceneUrl);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	try {
						mp.prepare();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    return true;
                }
            });
            
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            	  @Override
            	  public void onCompletion(MediaPlayer mp) {
            		sceneState.videoIsNotPlaying(Music.this);
            		customToast(artistName, songTitle, album,networkAvatar);
            		customToast(artistName, songTitle, album,networkAvatar);
                  	customToastLogo();
                  	customToastLogo();
            		  mp.release();
            		  //TODO notPlaying();
                  	 // Intent welcome = new Intent(Music.this,TV.class);
                     // Music.this.startActivity(welcome);
                      Music.this.finish();
                      
            	    
            	  }
            	});
        player.prepareAsync();
        
        
        
    }catch (Exception e) {
        e.printStackTrace();
        // TODO: handle exception


    }
    	
    	
    }
    
	public void playing(){
		
	}
	
	public void notPlaying(){
			
	}

	 private void customToast(String artistName, String songTitle, String album, String avatar) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
		 Picasso.with(this).load(avatar).into(imageToast);
	 	 TextView xartistName = (TextView) toastLayout.findViewById(R.id.toastText1);
	 	 TextView xsongTitle = (TextView) toastLayout.findViewById(R.id.toastText3);
	 	 TextView xalbum = (TextView) toastLayout.findViewById(R.id.toastText4);
	 	 xartistName.setText(artistName);
	 	 xsongTitle.setText(songTitle);
	 	 xalbum.setText(album);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM|Gravity.RIGHT,100,50);
	 	 toast.show();
	 }
	 
	 
	 private void customToastLogo() {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toastlogo, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 ImageView imageToast = (ImageView) toastLayout.findViewById(R.id.toastImage);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.setGravity(Gravity.TOP|Gravity.RIGHT,75,75);
	 	 toast.show();
	 }
	 


    
}