package tv.loqoo.FourthScreen;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

import tv.loqoo.FourthScreen.Channels.Channel_0001;
import tv.loqoo.FourthScreen.Services.PublicSessionTransportService;
import tv.loqoo.FourthScreen.Services.TransportService;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.KeyValueStore;
import tv.loqoo.FourthScreen.Utils.LTVID;
import tv.loqoo.FourthScreen.Utils.PubnubServices;
import tv.loqoo.FourthScreen.Utils.RestartApp;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

//import com.yume.android.sdk.YuMeSDKInterfaceImpl; 
//import com.yume.android.sdk.YuMeSDKInterface;


//import com.yume.android.sdk.YuMeAdParams; 
//import com.yume.android.sdk.YuMeException; 
//import com.yume.android.sdk.YuMeStorageMode; 



public class Start extends Activity {
	
	public String TAG = "loqootv";
	private boolean isOwnerAPaidSubscriber;
	private boolean isTVLogged;
	private boolean isTVRegisteredWithARemote;
	private boolean isPublic;
	private Handler handler;
	private String sessionId;
	Context context;
	private String channel;
	@InjectView(R.id.startupCode) TextView startupCode;
	ViewGroup rootContainer;
	Scene scene1;
	Scene scene2;
	Transition transitionManager;
	String AppPrefs ="LoQooTVPrefs";
	DeviceInfo dvinfo;
	public static int ok = 0;

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.v(TAG, "start");
		if(android.os.Build.VERSION.SDK_INT >=
				android.os.Build.VERSION_CODES.KITKAT){
			setContentView(R.layout.root);
			rootContainer = 
	                (ViewGroup) findViewById(R.id.root_layout);
		
			scene1 = Scene.getSceneForLayout(rootContainer, 
	                   R.layout.start, this);
		
			scene2 = Scene.getSceneForLayout(rootContainer, 
	                    R.layout.standard_channel_layout, this);
		
			scene1.enter();
			
		}else{
			
			setContentView(R.layout.start);
		}
		
		fullscreen();

		ButterKnife.inject(this);
		LTVID ltvid = new LTVID();
		String xsessionId = ltvid.publicLTVID();
		sessionId = xsessionId.substring(0, xsessionId.length() - 7);
		Log.v(TAG+sessionId,sessionId);
		SharedPreferences sp = getSharedPreferences(AppPrefs, Start.this.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("sessionId", sessionId);
		Intent serviceIntent = new Intent(this, PublicSessionTransportService.class);
        serviceIntent.putExtra("channelName",sessionId);
        Log.d("startSession.ServiceIntent", sessionId);
        startService(serviceIntent);
        
		  ImageView centerLogo = (ImageView) this.findViewById(R.id.start_center_logo);
		  Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
		  centerLogo.startAnimation(pulse);
		// Register the listener with the Location Manager to receive location updates
		  LocationManager locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
		  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		  Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		  //Log.v(TAG,lastKnownLocation.toString());
		  
		  
		  Log.d("publicSingleSessionID",sessionId);
		  startupCode.setText(sessionId);
		  //start(DeviceInfo.tvId(this));
		  
	        handler = new Handler();
	        handler.postDelayed(new Runnable() {
	            @Override
	            public void run() {
	            	startupCode.setVisibility(View.VISIBLE);
	            	startupCode.animate().translationXBy(115).start();

	            }
	        }, 5000);
	        
	        handler = new Handler();
	        handler.postDelayed(new Runnable() {
	            @Override
	            public void run() {
	            	
	            	Intent i = new Intent(Start.this, Channel_0001.class);
	            	startActivity(i);
	            	//Start.this.stopService(new Intent(Start.this, PublicSessionTransportService.class));
	            	//RestartApp.restart(Start.this);

	            }
	        }, 10000);


}
	
	public void goToScene2 (View view)
	{
		TransitionManager.go(scene2, transitionManager);
	}
	
	public void goToScene1 (View view)
	{
		TransitionManager.go(scene1, transitionManager);
	}
	
	public void fullscreen(){
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        		| View.SYSTEM_UI_FLAG_IMMERSIVE;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView()
            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
        (new View.OnSystemUiVisibilityChangeListener() {
    @Override
    public void onSystemUiVisibilityChange(int visibility) {
        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
        	}else{}
    	}
        });
	}
	
	
	LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	    Log.v(TAG,location.toString());
	    }

	    public void onStatusChanged(String provider, int status, Bundle extras) {
	    	Log.v(TAG, provider);
	    }

	    public void onProviderEnabled(String provider) {
	    	Log.v(TAG, provider);
	    	
	    }

	    public void onProviderDisabled(String provider) {
	    	
	    }


	  };
	  

	
	
	public void start(String remoteid) {
		if (isTVLogged){

			//PART 1a - Genesis
			//ADVANCE COUNTERS, SHOW PRESENCE, WELCOME BACK PRESENT SOMETHING FROM THE PAST
			//GOOD MORNING INTRO TYPE, GROUP ANNOUNCEMENTS
			//GET TV HISTORY
			
            PubnubServices pubnubServices = new PubnubServices(this);                                             
            new DeviceInfo(this);   
            String type = "isTVLogged";
            JSONObject subdata = DeviceInfo.getSubscriberData(this);
            try{                                                      

                    subdata.put("sceneType", type);

            }catch(JSONException e){                                  
                    Log.v(TAG+"isTVLogged",e.toString());   
            };                                                        
            pubnubServices.sendmsgJSON("pubnubController", subdata);

			if (isPublic == true){

				//Part 1b - A Public TV
				//SHOW PUBLIC WAY TO INTERFACE WITH TV


			}else{

				if (isTVRegisteredWithARemote){

					//PART 2a - inSync
					//SEND SOMETHING, DO SOMETHING ON THE REMOTE TO LET USER KNOW, TV AND PHONE IS
					//CONNECTED  BASISCALLY THIS IS THE SYNC PROCESS
					//GET REMOTE APP HISTORY, MORE GROUP ANNOUNCEMENTS

					if (isOwnerAPaidSubscriber){

						//PART 3a - Show Me The Money/Offering
						//PLACE CERTAIN RESTRICTIONS ON ACCESS
						//INFORM USER OF LIMITATIONS AND PAST USAGE
						//AND FUTURE LOQOOTV DEVELOPMENTS, GIVE AN OFFER!


						//START TV...GO TO NEXT SCREEN


					}else {
						//PART 3b
						//ENTICE OWNER/USER TO SPEND MONEY ON SERVICE
						//SHOW ADS ON TV AND ON REMOTE
					}

				}else {
					//PART 2b
					//INFORM OWNER TO DOWNLOAD LOQOOTV REMOTE AND REGISTER TV WITH IT
				}

			}

		}else{
			//PART 1c
			//LOG TV's DEVICEINFO INTO SYSTEM, PLEASE WAIT WHILE WE.....
			//WHEN FINISH CALL start() again



		}
	}

	protected void makeUseOfNewLocation(Location location) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
