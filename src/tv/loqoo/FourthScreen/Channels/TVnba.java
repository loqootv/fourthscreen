package tv.loqoo.FourthScreen.Channels;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Fragments.FragFooter;
import tv.loqoo.FourthScreen.Fragments.FragExtra;
import tv.loqoo.FourthScreen.Fragments.FragCenterLogo;
import tv.loqoo.FourthScreen.Fragments.FragTop;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragVideo;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class TVnba extends Activity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     
        setContentView(R.layout.listview);
        View view = findViewById(android.R.id.content);
        Animation mLoadAnimation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        mLoadAnimation.setDuration(8000);
        view.startAnimation(mLoadAnimation);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragTop fragTop = new FragTop();
        FragFooter fragBottom = new FragFooter();
        FragCenterLogo fragMain = new FragCenterLogo();
        //FragExtra fragExtra = new FragExtra();
        fragmentTransaction.add(R.id.top_fragment, fragTop);
        fragmentTransaction.add(R.id.bottom_fragment, fragBottom);
        fragmentTransaction.add(R.id.main_fragment, fragMain);
        //fragmentTransaction.add(R.id.extra_fragment, fragExtra);
        fragmentTransaction.commit();
        
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		finish();
	}
}