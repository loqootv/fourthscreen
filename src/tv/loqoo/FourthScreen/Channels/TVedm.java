package tv.loqoo.FourthScreen.Channels;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Fragments.FragCardsList;
import tv.loqoo.FourthScreen.Fragments.FragCenterLogo;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragVideo;
import tv.loqoo.FourthScreen.R.anim;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import tv.loqoo.FourthScreen.Services.TransportService;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

public class TVedm extends Activity {
	
	    public loqooBroadcast mloqooBroadcast = null;
	    JSONObject message = null;
	    EditText emailTwitter;
		EditText hiddenPassword;
		String EmailTwitter;
		String networkName;
		String HiddenPassword;
		String latitude;
		String longitude;
		Boolean isBeta = true;
		String xincomingScene;
		String output;
		String Tag1;
		String Tag3;
		String sceneLat;
		String sceneLong;
		String channelName;
	    String networkAvatar;
	    String networkPassword;
	    String sceneLikes;
		String sceneCreatorWebpage;
		String sceneTitle;
		String sceneDesc;
		String sceneCreatorProfileUrl;
		String sceneViews;
		String scenePurchaseLink;
		String sceneImageUrl;
		String sceneCreatorProfileImageUrl;
		String sceneProvider;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent serviceIntent = new Intent(this, TransportService.class);
        startService(serviceIntent);
        mloqooBroadcast = new loqooBroadcast();
        setContentView(R.layout.standard_channel_layout);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragCenterLogo fragMain = new FragCenterLogo();
        //FragExtra fragExtra = new FragExtra();
        fragmentTransaction.add(R.id.pip_fragment, fragMain);
        //fragmentTransaction.add(R.id.extra_fragment, fragExtra);
        fragmentTransaction.commit();
     
       
        
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("LifeCycle.TVsix","ONPause");
		unregisterReceiver(mloqooBroadcast);
		//unregisterReceiver(mGeotriggerBroadcastReceiver);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d("LifeCycle.TVsix","ONresume");
    	IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE");
    	registerReceiver(mloqooBroadcast, intentFilter);
		Log.d("LifeCycle.TVsix.ONresume","register_receiever");
		
	}
	
	public class loqooBroadcast extends BroadcastReceiver{
	    @Override
	    public void onReceive(Context context, Intent intent) {
	    	Log.d("mloqooBroadcast","start_onRecieve_Insideactivity");
	    	Bundle args = new Bundle();
	    	Bundle args1 = new Bundle();
	        if (intent.getAction().equals("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE")) {
	            try{
				message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
				System.out.printf("mloqooBroadcast",message);
				String incomingScene = message.getString("url");
				Log.d("mloqooBroadcast", incomingScene);
				output = incomingScene.replaceFirst("s","");
				String sceneProvider = message.getString("sceneProvider");
				if(sceneProvider.equalsIgnoreCase("loqootv") || (sceneProvider.equalsIgnoreCase("youtubeScene"))){
					sceneProvider = message.getString("sceneProvider");
					networkName = message.getString("networkName");
		            networkAvatar = message.getString("networkAvatar");
		            Tag1 = message.getString("sceneTag1");
		            Tag3 = message.getString("sceneTag3");
		            sceneLat = message.getString("sceneLat");
		            sceneLong = message.getString("sceneLong");
		            args.putString("sceneProvider", sceneProvider);
		            args.putString("url", output);
		            args.putString("networkName", networkName);
		            args.putString("networkAvatar", networkAvatar);
		            args.putString("Tag1", Tag1);
		            args.putString("Tag3", Tag3);
		            args.putString("sceneLat", sceneLat);
		            args.putString("sceneLong", sceneLong);
				}else {
					networkName = message.getString("networkName");
					sceneLikes = message.getString("sceneLikes");
					sceneCreatorWebpage = message.getString("sceneCreatorWebpage");
					sceneTitle = message.getString("sceneTitle");
					sceneDesc = message.getString("sceneDesc");
					sceneCreatorProfileUrl = message.getString("sceneCreatorProfileUrl");
					sceneViews = message.getString("sceneViews");
					scenePurchaseLink = message.getString("scenePurchaseLink");
					sceneImageUrl = message.getString("sceneImageUrl");
					sceneCreatorProfileImageUrl = message.getString("sceneCreatorProfileImageUrl");
		            networkAvatar = message.getString("networkAvatar");
		            Tag1 = message.getString("sceneTag1");
		            Tag3 = message.getString("sceneTag3");
		            sceneLat = message.getString("sceneLat");
		            sceneLong = message.getString("sceneLong");
		            args.putString("url", message.getString("output"));
		            args.putString("sceneProvider", message.getString("sceneProvider"));
		            args.putString("networkName", message.getString("networkName"));
		            args.putString("sceneLikes", message.getString("sceneLikes"));
		            args.putString("sceneCreatorWebpage", message.getString("sceneCreatorWebpage"));
		            args.putString("sceneTitle", message.getString("sceneTitle"));
		            args.putString("sceneDesc", message.getString("sceneDesc"));
		            args.putString("sceneCreatorProfileUrl", message.getString("sceneCreatorProfileUrl"));
		            args.putString("sceneViews", message.getString("sceneViews"));
		            args.putString("scenePurchaseLink", message.getString("scenePurchaseLink"));
		            args.putString("sceneImageUrl", message.getString("sceneImageUrl"));
		            args.putString("sceneCreatorProfileImageUrl", message.getString("sceneCreatorProfileImageUrl"));
		            args.putString("networkAvatar", message.getString("networkAvatar"));
		            args.putString("Tag1", message.getString("Tag1"));
		            args.putString("Tag3", message.getString("Tag3"));
		            args.putString("sceneLat", message.getString("sceneLat"));
		            args.putString("sceneLong", message.getString("sceneLong"));
				}
	            }catch(JSONException e){
	            	System.out.printf("fScreen.IncomingScene.Error", e);	
	            }

	        	Log.d("mloqooBroadcast","insideactivity");
	        	FragmentManager fragmentManager = getFragmentManager();
	            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
	            FragVideo fragVideo = new FragVideo();
	            fragVideo.setArguments(args);
	            fragmentTransaction.replace(R.id.pip_fragment, fragVideo, "video"); 
	            fragmentTransaction.addToBackStack(null);
	            fragmentTransaction.commit();

	        }
	    }
	};
}