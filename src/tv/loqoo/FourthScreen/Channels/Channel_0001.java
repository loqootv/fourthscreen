package tv.loqoo.FourthScreen.Channels;
 

import tv.loqoo.FourthScreen.Utils.KeyValueStore;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.ReturnData;
import tv.loqoo.FourthScreen.Fragments.FragFooter;
import tv.loqoo.FourthScreen.Fragments.FragCornerLogo;
import tv.loqoo.FourthScreen.Fragments.FragCenterLogo;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragAudio;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragImage;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragVideo;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragText;
import tv.loqoo.FourthScreen.Fragments.Scenes.FragWebView;
import tv.loqoo.FourthScreen.LoqooTVApplication;
import tv.loqoo.FourthScreen.R.anim;
import tv.loqoo.FourthScreen.R.id;
import tv.loqoo.FourthScreen.R.layout;
import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import tv.loqoo.FourthScreen.Services.TransportService;
import tv.loqoo.FourthScreen.Services.PublicSessionTransportService;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.newrelic.agent.android.NewRelic;



public class Channel_0001 extends Activity implements 
FragText.OnFragTextListener,FragAudio.OnFragAudioListener,
FragVideo.OnFragVideoListener, FragImage.OnFragImageListener,
FragWebView.OnFragWebViewListener{
	
		String TAG = "loqootv";
	    public loqooBroadcast mloqooBroadcast = null;
	    JSONObject message = null;
	    EditText emailTwitter;
		EditText hiddenPassword;
		String EmailTwitter;
		String networkName;
		String HiddenPassword;
		String latitude;
		String longitude;
		Boolean isBeta = true;
		String incomingScene;
		String output;
		String sceneUrl;
		String body;
		String profileNAME;
		String profileIMAGE;
		String sceneTITLE;
		String sceneVIEWS;
		String sceneLIKES;
		String sceneTHUMBNAIL;
		String scenePROVIDER;
		String sceneDURATION;
		String sceneCHANNEL;
		String sceneACTION;
		String value;
		String channel;
		String centerLogoUrl = "http://loqootv.s3.amazonaws.com/ltvrobotsonly.png" ;
		String cornerLogoUrl = "http://loqootv.s3.amazonaws.com/ltvicon.png";
		PowerManager pm;
		KeyValueStore kvstore;
		FragmentManager fragmentManager;
		FragmentTransaction fragmentTransaction;
		FragVideo fragVideo = new FragVideo();
        FragImage fragImage = new FragImage();
        FragAudio fragAudio = new FragAudio();
        FragFooter fragFooter = new FragFooter();
        FragCenterLogo fragCenterLogo = new FragCenterLogo();
        FragCornerLogo fragCornerLogo = new FragCornerLogo();
        PopupWindow popup;
        Boolean isplaying = false;
	
	@Override
	protected void onStart() {
		super.onStart();
	}
		
	@SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.standard_channel_layout);
        fullscreen();

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "loqootv_wakelock");
        wl.acquire();
        
    	
    	//NewRelic TEST
        NewRelic.withApplicationToken(
        		"AAe77d9e2c3bba40c3aeb4776ce1a3ee1208384dc8"
        		).start(this.getApplication());
        
        Intent intent = getIntent();
        channel = intent.getStringExtra("channel");
        centerLogoUrl = intent.getStringExtra("centerLogoUrl");
        if (centerLogoUrl == null){
        	centerLogoUrl = "http://loqootv.s3.amazonaws.com/ltvicon.png";
        }
        cornerLogoUrl = intent.getStringExtra("cornerLogoUrl");
        if (cornerLogoUrl == null){
        	cornerLogoUrl = "http://loqootv.s3.amazonaws.com/ltvrobotsonly.png";
        }
        
        Log.v(TAG+"centerLogoUrl", centerLogoUrl);
        Log.v(TAG+"cornerLogoUrl", cornerLogoUrl);

        
        if(channel != null){
		KeyValueStore kvstore = new KeyValueStore(this);
		kvstore.setCurrentChannel(channel);
		Intent serviceIntent = new Intent(this, TransportService.class);
        serviceIntent.putExtra("channelName",channel);
        Log.d("channel0001.ServiceIntent", channel);
        startService(serviceIntent);
        
        Intent serviceIntent2 = new Intent(this, AdministrativeTransportService.class);
        serviceIntent.putExtra("channelName","LTV");
        Log.d("channel0001.ServiceIntent2", channel);
        startService(serviceIntent2);
        
        
        }else {
        	channel = "@loqootv";
    		KeyValueStore kvstore = new KeyValueStore(this);
    		kvstore.setCurrentChannel(channel);
    		Intent serviceIntent = new Intent(this, TransportService.class);
            serviceIntent.putExtra("channelName",channel);
            Log.d("channel0001.ServiceIntent", channel);
            startService(serviceIntent);
        }
        
        mloqooBroadcast = new loqooBroadcast();
        
        //SETUP INTIAL FRAGMENTS
        Bundle argsFooter = new Bundle();
        Bundle argsCenterLogo = new Bundle();
        Bundle argsCornerLogo = new Bundle();
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragCenterLogo = new FragCenterLogo();
        fragFooter = new FragFooter();
        fragCornerLogo = new FragCornerLogo();
        String FragFooterMsg = "Txt a Pic, Video or YouTube |SoundCloud | Instagram Link to 714-988-6869 and #MAKEASCENE!" +
        		" LIVE on, "+"@LoQooTV";
        argsFooter.putString("fragFooter",FragFooterMsg);
        argsCenterLogo.putString("centerLogoUrl", centerLogoUrl);
        argsCornerLogo.putString("cornerLogoUrl", cornerLogoUrl);
        fragFooter.setArguments(argsFooter);
        fragCenterLogo.setArguments(argsCenterLogo);
        fragCornerLogo.setArguments(argsCornerLogo);
        fragmentTransaction.add(R.id.pip_fragment, fragCenterLogo);
        fragmentTransaction.add(R.id.logo_fragment, fragCornerLogo);
        fragmentTransaction.add(R.id.footer_fragment, fragFooter);
        fragmentTransaction.commit();
        /**
        popup = new PopupWindow(this);
        popup.showAtLocation(mainLayout, Gravity.BOTTOM, 10, 10);
        popup.update(50, 50, 300, 80);
        params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        tv.setText("Hi this is a sample text for popup window");
        layout.addView(tv, params);
        popUp.setContentView(layout);
        // popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10);
        mainLayout.addView(but, params);
        setContentView(mainLayout);
        **/
	}
		
	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("LifeCycle.TV.onPause","ONPause");
		unregisterReceiver(mloqooBroadcast);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		fullscreen();
		Log.d("LifeCycle.TV.onResume","onResume");
    	IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE");
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_AUDIO_SCENE");
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_IMAGE_SCENE");
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_COMMENT_SCENE");
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_MAP_SCENE");
        intentFilter.addAction("tv.loqoo.v4.ORIGINAL_WWW_SCENE");
        intentFilter.addAction("tv.loqoo.v4.NEWS_FLASH");
    	registerReceiver(mloqooBroadcast, intentFilter);
		Log.d("LifeCycle.LTV.ONresume","register_receiever");
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        	fullscreen();
          if(!hasFocus){
          Log.d("TV.channel001", "LOST FOCUS");

          Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
          sendBroadcast(closeDialog);
          }
      }
	
	public void fullscreen(){
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        		| View.SYSTEM_UI_FLAG_IMMERSIVE;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView()
            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
        (new View.OnSystemUiVisibilityChangeListener() {
    @Override
    public void onSystemUiVisibilityChange(int visibility) {
        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
        	}else{}
    	}
        });
	}
	
	public class loqooBroadcast extends BroadcastReceiver{
	    @Override
	    public void onReceive(Context context, Intent intent) {
	    	if (!isplaying){
	    	Log.d("mloqooBroadcast","start_onRecieve_Insideactivity");
	    	Bundle argsVideo = new Bundle();
	    	Bundle argsAudio = new Bundle();
	    	Bundle argsImage = new Bundle();
	    	Bundle argsMeta = new Bundle();
	        if (intent.getAction().equals("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE")) {
	        	Log.d("TV.channel001.videoScene", "starting videoScene");
	        	incomingScene(intent,message,argsVideo,argsMeta,fragVideo,"video");
	        }else if(intent.getAction().equals("tv.loqoo.v4.ORIGINAL_AUDIO_SCENE")){
	        	Log.d("TV.channel001.audioScene", "starting audioScene");
				incomingScene(intent,message,argsAudio,argsMeta,fragAudio,"audio");
	        }else if(intent.getAction().equals("tv.loqoo.v4.ORIGINAL_IMAGE_SCENE")){
	        	Log.d("TV.channel001.imageScene", "starting imageScene");
		        incomingScene(intent,message,argsImage,argsMeta,fragImage,"image");
	        }else if(intent.getAction().equals("tv.loqoo.v4.NEWS_FLASH")){
	        	Log.d("TV.channel001.NewsFlash", "starting NewsFlash");
	        	incomingNewsFlash(intent,message);
	        }else if(intent.getAction().equals("tv.loqoo.v4.ORIGINAL_WWW_SCENE")){
	        	incomingWebView(intent,message);
	        }
	    	}else{
	    		Log.v(TAG,"is already playing store the scene for later");
	    	}
	    }
	};
	
		
	public void incomingScene(Intent intent, JSONObject message, Bundle args, Bundle args1,
			Fragment frag, String string){
		try{
		message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
		System.out.printf("Channel_0001_BroadcastReceiver_RECEIVED_MSG",message);
		channel = message.getString("channel");
		sceneACTION = intent.getAction().toString();
		Intent serviceIntent = new Intent(this, TransportService.class);
        serviceIntent.putExtra("channelName",channel);
        stopService(serviceIntent);
        Log.d("channelTransportSTOPPED", channel);
		startService(serviceIntent);
		Log.d("channelTransportSTARTED", channel);
		incomingScene = message.getString("url");
		Log.d("mloqooBroadcast", incomingScene);
		output = incomingScene.replaceFirst("s","");
		//String sceneTHUMBNAIL = message.getString("sceneTHUMBNAIL");
		//String profileNAME = message.getString("profileNAME");
		String sceneDURATION = message.getString("sceneDURATION");
		String sceneTITLE = message.getString("sceneTITLE");
		String sceneVIEWS = message.getString("sceneVIEWS");
		String sceneLIKES = message.getString("sceneLIKES");
		String body = message.getString("body");
        //args.putString("profileIMAGE", profileIMAGE);
        //argsAudio.putString("sceneTHUMBNAIL", sceneTHUMBNAIL);
        args.putString("url", incomingScene);
        args.putString("profileNAME", profileNAME);
        args.putString("scenePROVIDER", scenePROVIDER);
        args.putString("sceneTITLE", sceneTITLE);
        args.putString("sceneVIEWS", sceneVIEWS);
        args.putString("sceneLIKES", sceneLIKES);
        args.putString("sceneDURATION", sceneDURATION);
        args.putString("body", body);
        args.putString("sceneCHANNEL", sceneCHANNEL);
        args.putString("sceneACTION", sceneACTION);
        String fragFooter = "Title:"+sceneTITLE+"Views"+sceneVIEWS+"FROM"+profileNAME;
        args1.putString("fragFooter", fragFooter);
		}catch(JSONException e){
			System.out.printf("TV.channel001.incomingscene.Error", e);
		}
		FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
        FragFooter fragFooter = new FragFooter();
        frag.setArguments(args);
        fragFooter.setArguments(args1);
        fragmentTransaction.replace(R.id.pip_fragment, frag, string);
        fragFooter.setArguments(args1);
        fragmentTransaction.replace(R.id.footer_fragment, fragFooter, "metadata");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
	}
	
	public void incomingNewsFlash(Intent intent, JSONObject message){
		Bundle args = new Bundle();
		try{
			message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
			System.out.printf("Channel_0001_BroadcastReceiver_RECEIVED_MSG",message);
			channel = message.getString("channel");
			sceneACTION = intent.getAction().toString();
			Intent serviceIntent = new Intent(this, TransportService.class);
	        serviceIntent.putExtra("channelName",channel);
	        stopService(serviceIntent);
	        Log.d("channelTransportSTOPPED", channel);
			startService(serviceIntent);
			Log.d("channelTransportSTARTED", channel);
			incomingScene = message.getString("msg");
			Log.d("mloqooBroadcast", incomingScene);
			String fragFooter = incomingScene;
	        args.putString("fragFooter", fragFooter);
	        args.putString("sceneACTION", sceneACTION);
			}catch(JSONException e){
				System.out.printf("TV.channel001.incomingscene.NewsFlash_ERROR", e);
			} 
			FragmentManager fragmentManager = getFragmentManager();
	        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
	        FragFooter fragFooter = new FragFooter();
	        fragFooter.setArguments(args);
	        fragmentTransaction.replace(R.id.footer_fragment, fragFooter, "newsFlash");
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
		}	
	
	public void incomingWebView(Intent intent, JSONObject message){
		Bundle args = new Bundle();
		try{
			message = (JSONObject) new JSONTokener(intent.getStringExtra("message")).nextValue();
			System.out.printf("Channel_0001_BroadcastReceiver_RECEIVED_MSG",message);
			channel = message.getString("channel");
			Intent serviceIntent = new Intent(this, TransportService.class);
	        serviceIntent.putExtra("channelName",channel);
	        stopService(serviceIntent);
	        Log.d("channelTransportSTOPPED", channel);
			startService(serviceIntent);
			Log.d("channelTransportSTARTED", channel);
			incomingScene = message.getString("url");
	        args.putString("fragFooter", incomingScene);
			}catch(JSONException e){
				System.out.printf("TV.channel001.incomingscene.WEbView_ERROR", e);
			} 
			FragmentManager fragmentManager = getFragmentManager();
	        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit);
	        FragFooter fragFooter = new FragFooter();
	        fragFooter.setArguments(args);
	        fragmentTransaction.replace(R.id.pip_fragment, FragWebView.newInstance(message), "webview");
	        fragmentTransaction.replace(R.id.footer_fragment, fragFooter, "webview");
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
		}	
	
	public void onWindowFocusChanged1(boolean isTrue) {
        super.onWindowFocusChanged(isTrue);

        if (!isTrue) 
        { 
        Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

	public void onSceneEnd(String name){
		
	}
	
	public void isPlaying(int playing){
		switch(playing){
		case 0: isplaying = false ;
				break;
		case 1: isplaying = true;
				break;
		}
	}
	
	public void showActionBar(String string){
		
	}
	

	
	}