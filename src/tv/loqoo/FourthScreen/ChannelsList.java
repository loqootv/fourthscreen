package tv.loqoo.FourthScreen;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class ChannelsList {
	
	public static LoqooTVApplication application;
	Context mContext;
	
	
	
	public ChannelsList(Context ctx) {
	        this.mContext = ctx;
	}
	
	public String loadJSONFromAsset(String file) {
    String json = null;
    try {
        File f = new File("/data/data/" + mContext.getPackageName() + "/" + "app_Parse/"+file);
        FileInputStream is = new FileInputStream(f);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        json = new String(buffer, "UTF-8");
        Log.v("FILE", json);
    } catch (IOException ex) {
        ex.printStackTrace();
        return null;
    }
    return json;
}

	public void addChannelsToList() {
	//String currentUser = loadJSONFromAsset("currentUser");
	String currentInstallation = loadJSONFromAsset("currentInstallation");
	try {
		JSONObject obj = new JSONObject(currentInstallation);
		JSONObject object = obj.getJSONObject("data");
		String createdAt = object.getString("createdAt");
		String updatedAt = object.getString("updatedAt");
		String deviceType = object.getString("deviceType");
		String installationId = object.getString("installationId");
		String timeZone = object.getString("timeZone");
		JSONArray channels1 = object.getJSONArray("channels");
		if (channels1 != null) { 
			   for (int i=0;i<channels1.length();i++){
				String loqootvChannel = channels1.get(i).toString();
				System.out.println(loqootvChannel);
				if (loqootvChannel == "loqootv") {
					break;
				}else {
					//application.channels.add(channels1.get(i).toString());
				}
				System.out.println(channels1);
					}
				}
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	}
	
	
}