package tv.loqoo.FourthScreen;

import android.content.Context;
import android.content.SharedPreferences;


public class SceneState {
	
	public void videoIsPlaying(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putBoolean("videoisPlaying", true);
		editor.commit();

	}

	public void videoIsNotPlaying(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putBoolean("videoisPlaying", false);
		editor.commit();

	}

	public void letNetworkWhoSentTheSceneKnowImWatching(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
	}
	
	public void letChannelKnowImWatching(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences("networkState", 0);
	}
}