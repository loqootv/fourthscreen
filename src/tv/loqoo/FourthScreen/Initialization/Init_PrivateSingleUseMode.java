package tv.loqoo.FourthScreen.Initialization;

import butterknife.ButterKnife;
import butterknife.InjectView;

import tv.loqoo.FourthScreen.Utils.LTVID;
import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Channels.Channel_0001;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

public class Init_PrivateSingleUseMode extends Activity {
	
	public boolean registered;
	public SharedPreferences sharedPrefs;
    public String ltvIDString;
    int delayMillis = 3000;
    Handler handler = new Handler();
    @InjectView(R.id.ltvRobotsLogoPrivateInit) ImageView ltvRobotsLogoPrivateInit;	
	@InjectView(R.id.ltvIDTextPrivateInit) TextView ltvIDTextPrivateInit;
	@InjectView(R.id.instructionsTextPrivateInit) TextView instructionsTextPrivateInit;
    
	@Override
	protected void onStart() {
		super.onStart();
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
	}
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Window w = getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
	setContentView(R.layout.init_private);
	ButterKnife.inject(this);
    ltvRobotsLogoPrivateInit.postDelayed(new Runnable() {
    	public void run() {
    		ltvRobotsLogoPrivateInit.setVisibility(View.INVISIBLE);
    		ltvIDTextPrivateInit.setVisibility(View.VISIBLE);
    		instructionsTextPrivateInit.setVisibility(View.VISIBLE);
    		}
    	}, 3000);
    
	    

	if (!IsRegistered()){
		String id = new LTVID().privateLTVID(this);
		Log.i("Private_Init", id);
		ltvIDTextPrivateInit.setText(id);
		
	}else {
		Intent intent=new Intent(getApplicationContext(), Channel_0001.class);
    	startActivity(intent);
	}




	
}

public Boolean IsRegistered(){
	sharedPrefs = getSharedPreferences("init", 0);
	registered = (sharedPrefs.getBoolean("registered",false));
	if(registered==true)
		return true;

	return false;
}

public Boolean isOnline() {
	  ConnectivityManager cm = 
			  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	  NetworkInfo ni = cm.getActiveNetworkInfo();
	  if(ni != null && ni.isConnected())
		  return true;
		  
	  return false;
}
	
}


