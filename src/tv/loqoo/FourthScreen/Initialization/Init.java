package tv.loqoo.FourthScreen.Initialization;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Channels.Channel_0001;
import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import tv.loqoo.FourthScreen.Utils.LTVID;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class Init extends Activity {
	
	String PRIVATE_SINGLE_USE_MODE = "PrivateSingleUseMode";
	String PUBLIC_SINGLE_USE_MODE = "PublicSingleUseMode";
	String PUBLIC_SINGLE_USE_MODE2 = "PublicSingleUseMode2";
	String publicSessionId;
	
	@InjectView(R.id.buttonPrivateSingleUseMode) Button buttonPrivateSingleUseMode;	
	@InjectView(R.id.buttonPublicGroupMode) Button buttonPublicGroupMode;
	@InjectView(R.id.buttonPublicSingleUseMode) Button buttonPublicSingleUseMode;
	@InjectView(R.id.buttonPublicSingleUseMode2) Button buttonPublicSingleUseMode2;
	@InjectView(R.id.buttonPrivateGroupMode) Button buttonPrivateGroupMode;
    

	@Override
	protected void onStart() {
		super.onStart();
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
	}
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Window w = getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
	setContentView(R.layout.init);
	Intent serviceIntent = new Intent(this, AdministrativeTransportService.class);
	startService(serviceIntent);
	ButterKnife.inject(this);
	}

	@OnClick({R.id.buttonPrivateGroupMode, R.id.buttonPublicGroupMode, R.id.buttonPublicSingleUseMode, R.id.buttonPublicSingleUseMode2, R.id.buttonPrivateSingleUseMode})
	public void doOnClick(View v) {
		
		switch (v.getId()) {
		  case R.id.buttonPrivateSingleUseMode:
			  Intent privateSingleUseInit = new Intent(Init.this, PhoneNumberInput.class);
			  privateSingleUseInit.putExtra("Mode", PRIVATE_SINGLE_USE_MODE );
			  startActivity(privateSingleUseInit);
		   break;

		  case R.id.buttonPublicGroupMode:
			  Intent publicGroupInit = new Intent(Init.this, Channel_0001.class);
			  startActivity(publicGroupInit);
		   break;
		   
		  case R.id.buttonPublicSingleUseMode:
			  Intent publicSingleUseInit = new Intent(Init.this, PhoneNumberInput.class);
			  publicSingleUseInit.putExtra("Mode", PUBLIC_SINGLE_USE_MODE);
			  startActivity(publicSingleUseInit);
		   break;
		   
		  case R.id.buttonPublicSingleUseMode2:
			  publicSessionId = new LTVID().publicLTVID();
			  Log.d("publicSingleUseMode2",publicSessionId);
			  Intent intent = new Intent(this, Init_PublicSingleUseMode2.class);
			  intent.putExtra("publicSessionId", publicSessionId);
			  startActivity(intent);
		   break;
		   
		  case R.id.buttonPrivateGroupMode:
			  Intent privateGroupInit = new Intent(Init.this, Init_PrivateGroupMode.class);
			  startActivity(privateGroupInit);
		   break;
		  default:
		   break;
		  }
		
	}

}

