package tv.loqoo.FourthScreen.Initialization;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Initialization.Init;
import tv.loqoo.FourthScreen.Initialization.Init_PrivateSingleUseMode;
import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import tv.loqoo.FourthScreen.Utils.DeviceInfo;
import tv.loqoo.FourthScreen.Utils.KeyValueStore;
import tv.loqoo.FourthScreen.Utils.LTVID;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PhoneNumberInput extends Activity {
	
	@InjectView(R.id.phoneNumberEditText) EditText phoneNumberEditText;
	@InjectView(R.id.getInstallLinkButton) Button getInstallLink;
	KeyValueStore kvstore;
	DeviceInfo dvinfo;
	String Mode;
	String publicSessionId;
	String E164Format;
	String xphoneNumber;
	String tvid;
	String msg;
	
	@Override
	protected void onStart() {
		super.onStart();
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
	}
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Window w = getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
	setContentView(R.layout.phonenumber);
	ButterKnife.inject(this);
	Intent serviceIntent = new Intent(this, AdministrativeTransportService.class);
	startService(serviceIntent);
	Intent intent = getIntent();
	Mode = intent.getStringExtra("Mode");
	}

	@OnClick(R.id.getInstallLinkButton)
	public void getInstallLink(View v) {
		
		if(Mode.equalsIgnoreCase("PrivateSingleUseMode")) {
			if(validatePhoneNumber()){
				Intent intent = new Intent(this, Init_PrivateSingleUseMode.class);
				startActivity(intent);
			}
		}else {
			if(validatePhoneNumber()){
				
				publicSessionId = new LTVID().publicLTVID();
				Log.d("publicSingleUseMode",publicSessionId);
				Intent intent = new Intent(this, Init_PublicSingleUseMode.class);
				intent.putExtra("publicSessionId", publicSessionId);
				startActivity(intent);
			}	
			
			
		}

	}
	
	public boolean validatePhoneNumber(){
		
	    try {
	    					
					    	tvid = DeviceInfo.tvId(this);
							xphoneNumber = phoneNumberEditText.getText().toString();
							
	                        // Use the library’s functions
	                        PhoneNumberUtil phoneUtil = PhoneNumberUtil
	                                .getInstance();
	                        PhoneNumber phNumberProto = null;

	                        try {

	                            // I set the default region to PH (Philippines)
	                            // You can find your country code here http://www.iso.org/iso/country_names_and_code_elements
	                            phNumberProto = phoneUtil.parse(xphoneNumber, "US");

	                        } catch (NumberParseException e) {
	                            // if there’s any error
	                            System.err
	                                    .println("NumberParseException was thrown: "
	                                            + e.toString());
	                        }

	                        // check if the number is valid
	                        boolean isValid = phoneUtil
	                                .isValidNumber(phNumberProto);

	                        if (isValid) {

	                            // get the valid number’s international format
	                            E164Format = phoneUtil.format(
	                                    phNumberProto,
	                                    PhoneNumberFormat.E164);

	                            Toast.makeText(
	                                    getBaseContext(),
	                                    "Phone number VALID: " + E164Format,
	                                    Toast.LENGTH_SHORT).show();
	                            return true;

	                        }else {

	                            // prompt the user when the number is invalid
	                            Toast.makeText(
	                                    getBaseContext(),
	                                    "Phone number is INVALID: " + xphoneNumber,
	                                    Toast.LENGTH_SHORT).show();
	                        }
	    } catch (Exception e) {
            e.printStackTrace();
        }
		return false;
	    	
	    }
	

	
	public void onPause(){
		super.onPause();
		phoneNumberEditText.setText("");
		finish();
	}




}