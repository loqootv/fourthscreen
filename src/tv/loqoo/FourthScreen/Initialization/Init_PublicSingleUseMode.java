package tv.loqoo.FourthScreen.Initialization;

import butterknife.ButterKnife;
import butterknife.InjectView;
import tv.loqoo.FourthScreen.Utils.CountDownAnimation;
import tv.loqoo.FourthScreen.Utils.CountDownAnimation.CountDownListener;
import tv.loqoo.FourthScreen.Utils.LTVID;
import tv.loqoo.FourthScreen.R;
import tv.loqoo.FourthScreen.Channels.Channel_0001;
import tv.loqoo.FourthScreen.Services.AdministrativeTransportService;
import tv.loqoo.FourthScreen.Services.TransportService;
import tv.loqoo.FourthScreen.Services.PublicSessionTransportService;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

public class Init_PublicSingleUseMode extends Activity implements CountDownListener {
	
	public boolean registered;
	public SharedPreferences sharedPrefs;
    public String ltvIDString;
    public int COUNTDOWN = 300;
    int delayMillis = 3000;
    Intent serviceIntent;
    Handler handler = new Handler();
    CountDownAnimation countDownAnimation;
    @InjectView(R.id.ltvRobotsLogoPublicInit) ImageView ltvRobotsLogoPublicInit;
	@InjectView(R.id.instructionsTextPublicInit) TextView instructionsTextPublicInit;
	@InjectView(R.id.countdownPublicInit) TextView countdownPublicInit;
    
	@Override
	protected void onStart() {
		super.onStart();
        Window w = getWindow();
        int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        w.getDecorView().setSystemUiVisibility(mUIFlag);
	}
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Window w = getWindow();
    int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LOW_PROFILE
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    w.getDecorView().setSystemUiVisibility(mUIFlag);
	setContentView(R.layout.init_public2);
	ButterKnife.inject(this);
	    ltvRobotsLogoPublicInit.postDelayed(new Runnable() {
	    	public void run() {
	    		ltvRobotsLogoPublicInit.setVisibility(View.INVISIBLE);
	    		instructionsTextPublicInit.setVisibility(View.VISIBLE);
	    		countdownPublicInit.setVisibility(View.VISIBLE);
	    		}
	    	}, 3000);
	    

	if (!IsRegistered()){
		Intent fromPhoneNumberInput = getIntent();
		String id = fromPhoneNumberInput.getStringExtra("publicSessionId");
		Log.d("publicSingleSessionId",id);
		Intent intent = new Intent();
		intent.setClass(this, PublicSessionTransportService.class);
		this.stopService(intent);
		serviceIntent = new Intent(this, PublicSessionTransportService.class);
        serviceIntent.putExtra("publicSessionId",id);
        startService(serviceIntent);
        countDownAnimation = new CountDownAnimation(countdownPublicInit, COUNTDOWN);
        countDownAnimation.setCountDownListener(this);
        countDownAnimation.start();
		
	}else {
		Intent intent=new Intent(getApplicationContext(), Channel_0001.class);
    	startActivity(intent);
	}
	
}


	public Boolean IsRegistered(){
		sharedPrefs = getSharedPreferences("init", 0);
		registered = (sharedPrefs.getBoolean("registered",false));
		if(registered==true)
			return true;
	
		return false;
	}
	
	public Boolean isOnline() {
		  ConnectivityManager cm = 
				  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni = cm.getActiveNetworkInfo();
		  if(ni != null && ni.isConnected())
			  return true;
			  
		  return false;
	}
	
	@Override
	public void onCountDownEnd(CountDownAnimation animation) {
		Intent back = new Intent(this, Init.class);
		startActivity(back);
		finish();
	}
	
	@Override
	protected void onPause(){
	super.onPause();
	Log.d("LifeCycle.Public_Init","ONpause");
	countDownAnimation.cancel();
	finish();
	}
}


