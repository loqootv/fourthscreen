// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen.Fragments.Scenes;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class FragWebView$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Fragments.Scenes.FragWebView target, Object source) {
    View view;
    view = finder.findById(source, 2131492930);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492930' for field 'fragWeb' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.fragWeb = (android.webkit.WebView) view;
    view = finder.findById(source, 2131492931);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492931' for field 'fragWebProgressBar' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.fragWebProgressBar = (android.widget.ProgressBar) view;
  }

  public static void reset(tv.loqoo.FourthScreen.Fragments.Scenes.FragWebView target) {
    target.fragWeb = null;
    target.fragWebProgressBar = null;
  }
}
