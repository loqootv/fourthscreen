// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen.Initialization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class Init_PrivateSingleUseMode$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Initialization.Init_PrivateSingleUseMode target, Object source) {
    View view;
    view = finder.findById(source, 2131492946);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492946' for field 'ltvIDTextPrivateInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.ltvIDTextPrivateInit = (android.widget.TextView) view;
    view = finder.findById(source, 2131492947);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492947' for field 'instructionsTextPrivateInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.instructionsTextPrivateInit = (android.widget.TextView) view;
    view = finder.findById(source, 2131492945);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492945' for field 'ltvRobotsLogoPrivateInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.ltvRobotsLogoPrivateInit = (android.widget.ImageView) view;
  }

  public static void reset(tv.loqoo.FourthScreen.Initialization.Init_PrivateSingleUseMode target) {
    target.ltvIDTextPrivateInit = null;
    target.instructionsTextPrivateInit = null;
    target.ltvRobotsLogoPrivateInit = null;
  }
}
