// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen.Initialization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class Init$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Initialization.Init target, Object source) {
    View view;
    view = finder.findById(source, 2131492942);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492942' for field 'buttonPublicGroupMode' and method 'doOnClick' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.buttonPublicGroupMode = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.doOnClick(p0);
        }
      });
    view = finder.findById(source, 2131492944);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492944' for field 'buttonPublicSingleUseMode2' and method 'doOnClick' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.buttonPublicSingleUseMode2 = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.doOnClick(p0);
        }
      });
    view = finder.findById(source, 2131492940);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492940' for field 'buttonPrivateGroupMode' and method 'doOnClick' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.buttonPrivateGroupMode = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.doOnClick(p0);
        }
      });
    view = finder.findById(source, 2131492943);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492943' for field 'buttonPublicSingleUseMode' and method 'doOnClick' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.buttonPublicSingleUseMode = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.doOnClick(p0);
        }
      });
    view = finder.findById(source, 2131492939);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492939' for field 'buttonPrivateSingleUseMode' and method 'doOnClick' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.buttonPrivateSingleUseMode = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.doOnClick(p0);
        }
      });
  }

  public static void reset(tv.loqoo.FourthScreen.Initialization.Init target) {
    target.buttonPublicGroupMode = null;
    target.buttonPublicSingleUseMode2 = null;
    target.buttonPrivateGroupMode = null;
    target.buttonPublicSingleUseMode = null;
    target.buttonPrivateSingleUseMode = null;
  }
}
