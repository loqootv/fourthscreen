// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen.Initialization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class Init_PublicSingleUseMode$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Initialization.Init_PublicSingleUseMode target, Object source) {
    View view;
    view = finder.findById(source, 2131492951);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492951' for field 'countdownPublicInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.countdownPublicInit = (android.widget.TextView) view;
    view = finder.findById(source, 2131492950);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492950' for field 'instructionsTextPublicInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.instructionsTextPublicInit = (android.widget.TextView) view;
    view = finder.findById(source, 2131492948);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492948' for field 'ltvRobotsLogoPublicInit' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.ltvRobotsLogoPublicInit = (android.widget.ImageView) view;
  }

  public static void reset(tv.loqoo.FourthScreen.Initialization.Init_PublicSingleUseMode target) {
    target.countdownPublicInit = null;
    target.instructionsTextPublicInit = null;
    target.ltvRobotsLogoPublicInit = null;
  }
}
