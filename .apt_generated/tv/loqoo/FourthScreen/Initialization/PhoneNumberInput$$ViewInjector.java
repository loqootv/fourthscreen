// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen.Initialization;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class PhoneNumberInput$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Initialization.PhoneNumberInput target, Object source) {
    View view;
    view = finder.findById(source, 2131492978);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492978' for field 'phoneNumberEditText' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.phoneNumberEditText = (android.widget.EditText) view;
    view = finder.findById(source, 2131492979);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492979' for field 'getInstallLink' and method 'getInstallLink' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.getInstallLink = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.getInstallLink(p0);
        }
      });
  }

  public static void reset(tv.loqoo.FourthScreen.Initialization.PhoneNumberInput target) {
    target.phoneNumberEditText = null;
    target.getInstallLink = null;
  }
}
