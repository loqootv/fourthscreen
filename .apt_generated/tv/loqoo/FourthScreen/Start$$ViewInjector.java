// Generated code from Butter Knife. Do not modify!
package tv.loqoo.FourthScreen;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class Start$$ViewInjector {
  public static void inject(Finder finder, final tv.loqoo.FourthScreen.Start target, Object source) {
    View view;
    view = finder.findById(source, 2131492992);
    if (view == null) {
      throw new IllegalStateException("Required view with id '2131492992' for field 'startupCode' was not found. If this view is optional add '@Optional' annotation.");
    }
    target.startupCode = (android.widget.TextView) view;
  }

  public static void reset(tv.loqoo.FourthScreen.Start target) {
    target.startupCode = null;
  }
}
