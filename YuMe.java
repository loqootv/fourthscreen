package Ads;


import tv.loqoo.FourthScreen.Start;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import com.yume.android.sdk.YuMeAdBlockType;
import com.yume.android.sdk.YuMeAdEvent;
import com.yume.android.sdk.YuMeAdParams;
import com.yume.android.sdk.YuMeAppInterface;
import com.yume.android.sdk.YuMeException;
import com.yume.android.sdk.YuMeParentViewInfo;
import com.yume.android.sdk.YuMeSDKInterface;
import com.yume.android.sdk.YuMeSDKInterfaceImpl;
import com.yume.android.sdk.YuMeStorageMode;

public class YuMe implements YuMeAppInterface { 

	String TAG = "loqootv";
	private static YuMe yumeAppInterfaceImpl; 
	private static YuMeSDKInterface yumeSDKInterface = null; 
	private Start start;
	
	DisplayMetrics displayMetrics = new DisplayMetrics(); 
	
	private YuMe() { 
		/* instantiate the YuMe SDK */ if(Integer.parseInt(Build.VERSION.SDK) >= 8) { 
		if(yumeSDKInterface == null) { 
		yumeSDKInterface = new YuMeSDKInterfaceImpl(); 
		} else { 
			Log.e(TAG,"Android API Level < 8. Hence, not instantiating YuMe SDK."); 
			}
		}
	} 
		
	public static synchronized YuMe getYuMeAppInterfaceImpl() { 
		if(yumeAppInterfaceImpl == null) {
			/* create the yumeAppInterfaceImpl instance, if it doesn't exist */ 
			yumeAppInterfaceImpl = new YuMe(); 
		} 
		return yumeAppInterfaceImpl; 
	} 

	public void YuMe_EventListener(YuMeAdBlockType adBlockType, YuMeAdEvent 
			adEvent, String eventInfo) { 
			switch (adEvent) { 
			case AD_READY: 
			if(adBlockType == YuMeAdBlockType.PREROLL) Log.d(TAG, "AD READY (Preroll)"); 
			break; 
			
			case AD_AND_ASSETS_READY: 
			if(adBlockType == YuMeAdBlockType.PREROLL) Log.d(TAG,
					"AD AND ASSETS READY (Preroll)"); break;
					
			case AD_NOT_READY: 
			if(adBlockType == YuMeAdBlockType.PREROLL) Log.d(TAG, "AD NOT READY (Preroll)."); 
			break; 
			
			case AD_PRESENT: Log.d(TAG, "AD PRESENT"); 
			break; 
			
			case AD_PLAYING: Log.d(TAG, "AD PLAYING"); 
			break; 
			 
			case AD_ABSENT: Log.d(TAG, "AD ABSENT"); 
			break; 
			
			case AD_COMPLETED: Log.d(TAG, "AD COMPLETED");
			
			case AD_ERROR: Log.d(TAG, "AD ERROR"); 
			Log.e(TAG, "Error Info: " + eventInfo);  
			break; 
			
			case AD_EXPIRED: Log.d(TAG, "AD EXPIRED");
			break; 
			
			default:
			break; 
				} 
			} 
	
	public Context YuMe_GetApplicationContext() { 
		/* get the application context */ 
		return getApplicationContext(); 
		}
	
	public Context YuMe_GetActivityContext() {
		/* get the activity context */ 
		return getActivityContext(); 
		} 
	
	public YuMeParentViewInfo YuMe_GetParentViewInfo() { 
		Context appContext = getApplicationContext(); if(appContext != null) { 
		Display display = ((WindowManager)appContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
		 
		display.getMetrics(displayMetrics); 
		 
		YuMeParentViewInfo parentViewInfo = new YuMeParentViewInfo(); 
		parentViewInfo.width = displayMetrics.widthPixels; 
		parentViewInfo.height = displayMetrics.heightPixels; parentViewInfo.left = 0; 
		parentViewInfo.top = 0; 
		parentViewInfo.statusBarAndTitleBarHeight = Start.STATUS_BAR_AND_TITLE_BAR_HEIGHT; 
		return parentViewInfo; 
		} 
		return null;
	}
	
	public Context getActivityContext() { 
		/* get the ad activity context */ 
		if(start != null) 
		return start.getActivityContext(); 
		return null; 
		} 
	
	public Context getApplicationContext() { 
		if(homeView != null) 
		return homeView.getAppContext(); 
		else if(start!= null) 
		return start.getAppContext(); 
		return null;
	};
	
	public void sdkInit() { 
		try { 
		 
		/* create the ad params object and set the ad params */ 
		YuMeAdParams adParams = new YuMeAdParams(); 
		adParams.adServerUrl = "http://shadow01.yumenetworks.com/"; 
		adParams.domainId = "1535FyPGabIb"; 
		adParams.storageSize = (float)10.0; 
		 
		adParams.eStorageMode = YuMeStorageMode.EXTERNAL_STORAGE; 
		 
		yumeSDKInterface.YuMeSDK_Init(adParams, this); 
		 
		} catch (YuMeException e) { 
		 
		Log.e(TAG, "YuMeException: " + e.getMessage()); 
		 
		} 

	} ;
	 
	/** 
	* De-initializes the YuMe SDK. 
	*/ 
	 
	public void sdkDeInit() { try { yumeSDKInterface.YuMeSDK_DeInit(); 
	} catch (YuMeException e) { 
	 
	Log.e(TAG, "YuMeException: " + e.getMessage()); 
	 
	} 
	 
	yumeSDKInterface = null; 
	 
	} ; 
	 
	/** 
	 
	* Requests the YuMe SDK to prefetch an ad. 
	 
	 
	*/ 
	 
	public void sdkInitAd() {
		YuMeAdBlockType adBlockType = YuMeAdBlockType.PREROLL; 
		 
		try { 
		 
		yumeSDKInterface.YuMeSDK_InitAd(adBlockType); 
		 
		} catch (YuMeException e) { 
		 
		Log.e(TAG, "YuMeException: " + e.getMessage()); 
		 
		} 

		 
		/** 
		 
		* Requests the YuMe SDK to show a prefetched ad. 
		 
		*/ 
		 
		public void sdkShowAd() { 
		 
		YuMeAdBlockType adBlockType = YuMeAdBlockType.PREROLL; 

	}
		
		<!--
		/** Gets the Application


	
	
	if(yumeSDKInterface == null){ 
	yumeSDKInterface = new YuMeSDKInterfaceImpl(); 
	}
	
	YuMeAdParams adParams = new YuMeAdParams(); 
	adParams.adServerUrl = "http://shadow01.yumenetworks.com/"; 
	adParams.domainId = "1535FyPGabIb"; 
	adParams.storageSize = 10; //storage size in MBs 
	adParams.eStorageMode = YuMeStorageMode.EXTERNAL_STORAGE; 

	try { 
		yumeSDKInterface.YuMeSDK_Init(adParams, yumeAppInterfaceImpl); 
		} catch (YuMeException e) { 
		Log.e("Error", "YuMeException: " + e.getMessage());
		
		}
		
		try { 
			YuMeAdBlockType adBlockType = YuMeAdBlockType.PREROLL; 
			yumeSDKInterface.YuMeSDK_InitAd(adBlockType); 
			} catch (YuMeException e) { 
			Log.e("Error", "YuMeException: " + e.getMessage()); 
			}
		

}
		@Override
		public void YuMeApp_EventListener(YuMeAdBlockType arg0,
				YuMeAdEvent arg1, String arg2) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public Context YuMeApp_GetActivityContext() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public Context YuMeApp_GetApplicationContext() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public YuMeParentViewInfo YuMeApp_GetParentViewInfo() {
			// TODO Auto-generated method stub
			return null;
		}
	
}
-->

}
